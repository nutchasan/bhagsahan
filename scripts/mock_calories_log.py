#!/usr/bin/env python

from bhagsahan import models
import mongoengine as me
import datetime
import random


def mock_calories_log():
    print('Start Mock Calories Data')
    me.connect('bhagsahan_db')

    user = models.User.objects(firstname='Nutchasan').first()

    
    
    for i in range(30, 0, -1):
        cal_out = models.CaloriesOutLog()
        set_date = datetime.datetime.now() - datetime.timedelta(days=i)
        cal_out.user = user
        cal_out.calories_out = random.randint(2000, 2600)
        cal_out.created_date = set_date
        cal_out.save()
        


    print('End mock')
    
def mock_bmi_log():
    print('Start Mock BMI Data')
    me.connect('bhagsahan_db')

    user = models.User.objects(firstname='Nutchasan').first()

    

    for i in range(30, 0, -1):
        weight = random.randint(69, 70)
        height = 179 
        bmi_log = models.BMILog()
        set_date = datetime.datetime.now() - datetime.timedelta(days=i)
        bmi_log.user = user
        bmi_log.weight = weight 
        bmi_log.height = height
        bmi_log.bmi =weight/((height/100)**2)
        bmi_log.created_date = set_date
        bmi_log.save()

    print('End mock')

if __name__ == '__main__':
    mock_calories_log()
    mock_bmi_log()