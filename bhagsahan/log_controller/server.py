import asyncio
import datetime
import json
import logging
logger = logging.getLogger(__name__)

from bhagsahan import models

# from . import services
from . import history_log

class LogControllerServer:
    def __init__(self, settings):
        self.settings = settings
        models.init_mongoengine(settings)
        self.history_log = history_log.HistoryLogSave()

    async def process_save_log_controller(self):
        while self.running:
            time_check = self.settings['DAIRY_TIME_TO_SAVE_LOG']
            hour, minute = time_check.split(':')
            date = datetime.date.today()
            time = datetime.time(int(hour), int(minute), 0)
            time_set = datetime.datetime.combine(date, time)
            time_to_check = time_set - datetime.datetime.now()
            
            logger.debug('before sleep')
            await asyncio.sleep(time_to_check.seconds)
            logger.debug('start check data expired')
            self.history_log.save_bmi_user()
            self.history_log.save_calories_out_user()
            logger.debug('sleep 2 Hr')
            await asyncio.sleep(7200)

    async def set_up(self, loop):
        logging.basicConfig(
                format='%(asctime)s - %(name)s:%(levelname)s - %(message)s',
                datefmt='%d-%b-%y %H:%M:%S',
                level=logging.DEBUG,
                )

    def run(self):
        self.running = True
        loop = asyncio.get_event_loop()
        # loop.set_debug(True)
        loop.run_until_complete(self.set_up(loop))
        handle_save_log_task = loop.create_task(self.process_save_log_controller())

        try:
            loop.run_forever()
        except Exception as e:
            print(e)
            self.running = False
        finally:
            loop.close()

