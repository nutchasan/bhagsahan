from bhagsahan import models
# from mongoengine.queryset.visitor import Q
import logging
import datetime

logger = logging.getLogger(__name__)


class HistoryLogSave:
    def __init__(self):
        pass

    def save_bmi_user(self):
        logger.debug('Start to save BMI log dairy')
        users = models.User.objects()
        bmi = 0.0
        for user in users:
            bmi = user.weight/((user.height/100)**2)
            bmi_log = models.BMILog(user=user,
                                    weight=user.weight,
                                    height=user.height,
                                    bmi=bmi,
                                    created_date=datetime.datetime.now())
            bmi_log.save()
        logger.debug('Success to save data to bmi log')

    def save_calories_out_user(self):
        logger.debug('Start to save Calories out log dairy')
        users = models.User.objects()
        for user in users:
            cal_out_log = models.CaloriesOutLog(user=user,
                                                calories_out=user.calories_out,
                                                created_date=datetime.datetime.now())
            cal_out_log.save()
        logger.debug('Success to save data to Calories out log')
