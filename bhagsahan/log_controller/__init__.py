from .server import LogControllerServer


def create_server():
    from bhagsahan.utils import config
    settings = config.get_settings()
    server = LogControllerServer(settings)
    return server
