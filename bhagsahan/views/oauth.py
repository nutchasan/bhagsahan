from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask)
from .. import models
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Flask, redirect, url_for, session, request, jsonify
from authlib.client import OAuth2Session
from flask.json import jsonify
from flask_login import logout_user, current_user, login_required, login_user
import datetime
import os
import json

import fitbit

os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = '1'
module = Blueprint('oauth', __name__, url_prefix='/')


client_id = "22B6GC"
client_secret = "1bb2a9a69907bf043548103b27da3a83"
authorization_base_url = 'https://www.fitbit.com/oauth2/authorize'
token_url = 'https://api.fitbit.com/oauth2/token'
scope = ["activity", "heartrate", "location", "nutrition", "profile", "settings", "sleep", "social", "weight"]
prompt = 'login'


def get_right_dateFormat(offset: int = 0) -> str:
    return str((datetime.datetime.now() - datetime.timedelta(days=offset)).strftime("%Y-%m-%d"))

def get_data(token):
    today_date = get_right_dateFormat()
    url = 'https://api.fitbit.com/1/user/7GQHW5/activities/date/'+today_date+'.json'
    fitbit = OAuth2Session(client_id, client_secret, token=token)
    return fitbit.get(url).json()

def fetch_token():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    if not user:
        return redirect(url_for('user.initial_user'))
    token = models.OAuthToken.objects().get(user=user)
    return token.to_dict()
 


@module.route("/login_fitbit")
def login_fitbit():
    if current_user.is_authenticated:
        return redirect(url_for('main.dashboard'))
    fitbit = OAuth2Session(client_id, client_secret, scope=scope, prompt=prompt)
    uri, state = fitbit.create_authorization_url(authorization_base_url)
    return redirect(uri)


@module.route("/callback")
# @login_required
def callback():
    oauth_session = OAuth2Session(client_id, client_secret, scope=scope)  
    token = oauth_session.fetch_access_token(token_url, authorization_response=request.url)
	# authd_client = fitbit.Fitbit(client_id, client_secret, access_token=token['access_token'], refresh_token=token['refresh_token'])	
    authd_client = fitbit.Fitbit(client_id, client_secret, access_token=token['access_token'],
            refresh_token=token['refresh_token'])
    # print('profile>>>>>', authd_client.user_profile_get()['user'])
    user_profile = authd_client.user_profile_get()['user']
    user = models.User.objects(encoded_id=user_profile['encodedId']).first()
    if not user:
        user = models.User(firstname=user_profile['firstName'],
                               lastname=user_profile['lastName'],
                               age=user_profile['age'],
                               sex=user_profile['gender'].lower(),
                               weight=user_profile['weight'],
                               encoded_id=user_profile['encodedId'])

    
    activities = authd_client.activities()['summary']
    user.calories_out = activities['caloriesOut']
    user.save()
    oauth_token = models.OAuthToken.objects(user=user).first()
    if not oauth_token:
        oauth_token = models.OAuthToken(access_token=token['access_token'],
                                        expires_in=token['expires_in'],
                                        refresh_token=token['refresh_token'],
                                        scope=token['scope'],
                                        token_type=token['token_type'],
                                        user_id=token['user_id'],
                                        expires_at=token['expires_at'],
                                        user=user)

    else:
        oauth_token.access_token=token['access_token']
        oauth_token.expires_in=token['expires_in']
        oauth_token.refresh_token=token['refresh_token']
        oauth_token.scope=token['scope']
        oauth_token.token_type=token['token_type']
        oauth_token.user_id=token['user_id']
        oauth_token.expires_at=token['expires_at']

    oauth_token.save()

    login_user(user, remember=True)
    return redirect(url_for('user.initial_user'))
    # user = models.User.objects.get(id=current_user._get_current_object().id)
	# oauth_token = models.OAuthToken.objects(user=user).first()
    
    # if not user:

    # if token: 
    #     if not oauth_token:
    #         oauth_token = models.OAuthToken(access_token=token['access_token'],
    #                                         expires_in=token['expires_in'],
    #                                         refresh_token=token['refresh_token'],
    #                                         scope=token['scope'],
    #                                         token_type=token['token_type'],
    #                                         user_id=token['user_id'],
    #                                         expires_at=token['expires_at'],
    #                                         user=user)
    #         oauth_token.save()
    #     else:
    #         oauth_token.access_token=token['access_token']
    #         oauth_token.expires_in=token['expires_in']
    #         oauth_token.refresh_token=token['refresh_token']
    #         oauth_token.scope=token['scope']
    #         oauth_token.token_type=token['token_type']
    #         oauth_token.user_id=token['user_id']
    #         oauth_token.expires_at=token['expires_at']
    #         oauth_token.save()

    # data = get_data(fetch_token())
    # data_json = json.dumps(data)
    # parsed = json.loads(data_json)
    # parsed = parsed['summary']
    # cal_out = json.dumps(parsed['caloriesOut'], indent=4, sort_keys=True) 
    # user.calories_out = cal_out
    # user.save()
    
    # return redirect(url_for('main.index'))


@module.route("/get_fitbit")
def get_fitbit():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    oauth_token = models.OAuthToken.objects(user=user).first()
    authd_client = fitbit.Fitbit(client_id, client_secret, access_token=oauth_token.access_token,
            refresh_token=oauth_token.refresh_token)

    # print('profile>>>>>', authd_client.activities())
    try:
        activities = authd_client.activities()['summary']
    except:
        return redirect(url_for('oauth.login_fitbit'))
    
    user.calories_out = activities['caloriesOut']
    user.save()
    # data = None
    # return 'OK'
    # try:
    #     data = get_data(fetch_token())
    # except:
    #     return redirect(url_for('oauth.login_fitbit'))
        
    # data_json = json.dumps(data)
    # parsed = json.loads(data_json)
    # parsed = parsed['summary']
    # parsed_2 = json.dumps(parsed, indent=4, sort_keys=True)
    # cal_out = json.dumps(parsed['caloriesOut'], indent=4, sort_keys=True) 
    # print(parsed_2)
    

    # user.save()
    
    return redirect(url_for('main.dashboard'))
