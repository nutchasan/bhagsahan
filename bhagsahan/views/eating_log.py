from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask,
                   Response,
                   send_file)
from .. import models
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Flask, redirect, url_for, session, request, jsonify
from flask_login import logout_user, current_user, login_required


import datetime


module = Blueprint('eating_log', __name__, url_prefix='/')


@module.route('/eating_list')
@login_required
def eating_list():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    eating_data = models.EatingLog.objects(user=user).order_by('-id')
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('eating_log/eating_log.html',
                           eating_data=eating_data,
                           user=user)