from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask,
                   Response,
                   send_file)
from .. import models
from mongoengine.queryset.visitor import Q
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Flask, redirect, url_for, session, request, jsonify
from flask_login import logout_user, current_user, login_required

from bokeh.io import show
from bokeh.palettes import Category20c
from bokeh.plotting import figure
from bokeh.transform import cumsum
from bokeh.embed import components
from bokeh.palettes import Spectral6
from bokeh.models import (ColumnDataSource,
                          FactorRange,
                          LabelSet,
                          Legend,
                          HoverTool,
                          LabelSet,
                          LegendItem,
                          ranges)


import datetime

def make_pie_per_week():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    today = datetime.date.today()
    start_date = datetime.datetime.combine(today, datetime.time(0, 0))
    end_date = datetime.datetime.combine(today, datetime.time(23, 50))
    days = []
    calories = []
    color_list = []

    for i in range(7):
        num = 0
        if i > 0:
            num += 1
        start_date = start_date - datetime.timedelta(days=num)
        end_date = end_date - datetime.timedelta(days=num)
        
        calories_log = models.CaloriesOutLog.objects(Q(user=user) &
                                                     Q(created_date__lte=end_date) &
                                                     Q(created_date__gte=start_date)).order_by('-id')
        if calories_log:
            for item in calories_log:
                calories.append(item.calories_out)
                days.append(item.created_date.strftime('%A'))
                break

    calories = calories[::-1]
    days = days[::-1]
    for i in calories:
        if i >= 2600:
            color_list.append('#7cb342')
        else:
            color_list.append('#ef5350')

    p = figure(x_range=days,
               y_range= ranges.Range1d(start=0,end=3500),
               plot_height=250,
               title="Calories Out per week (7 days)",
               toolbar_location=None,
               tools="")

    p.vbar(x=days,
           top=calories,
           width=0.9,
           color=color_list)

    source = ColumnDataSource(dict(x=days,y=calories))
    labels = LabelSet(x='x', y='y', text='y', level='glyph',
             x_offset=-13.5, y_offset=0, source=source, render_mode='canvas')

    p.add_layout(labels)
    
    p.xgrid.grid_line_color = None
    p.y_range.start = 0

    return p


def make_pie_per_month():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    today = datetime.date.today()
    start_date = datetime.datetime.combine(today, datetime.time(0, 0))
    end_date = datetime.datetime.combine(today, datetime.time(23, 50))
    factors = []
    calories_data = []
    color_list = []

    for i in range(28):
        num = 0
        if i > 0:
            num += 1
        start_date = start_date - datetime.timedelta(days=num)
        end_date = end_date - datetime.timedelta(days=num)

        calories_log = models.CaloriesOutLog.objects(Q(user=user) &
                                                     Q(created_date__lte=end_date) &
                                                     Q(created_date__gte=start_date)).order_by('-id')
        
        if calories_log:
            for item in calories_log:
                data_tuple = ()
                if i <= 6: 
                    data_tuple = ('Week 4', item.created_date.strftime('%d-%m-%Y'))
                    factors.append(data_tuple)
                    calories_data.append(item.calories_out)
                    break
                if i <= 13: 
                    data_tuple = ('Week 3', item.created_date.strftime('%d-%m-%Y'))
                    factors.append(data_tuple)
                    calories_data.append(item.calories_out)
                    break
                if i <= 20: 
                    data_tuple = ('Week 2', item.created_date.strftime('%d-%m-%Y'))
                    factors.append(data_tuple)
                    calories_data.append(item.calories_out)
                    break
                if i <= 27: 
                    data_tuple = ('Week 1', item.created_date.strftime('%d-%m-%Y'))
                    factors.append(data_tuple)
                    calories_data.append(item.calories_out)
                    break
    
    calories_data = calories_data[::-1]
    factors = factors[::-1]
    for i in calories_data:
        if i >= 2600:
            color_list.append('#7cb342')
        else:
            color_list.append('#ef5350')

    p = figure(x_range=FactorRange(*factors),
               y_range= ranges.Range1d(start=0,end=3500),
               plot_height=250,
               toolbar_location=None,
               tools="",
               title="Calories Out per month (28 days)",)

    p.vbar(x=factors,
           top=calories_data,
           width=0.9,
           color=color_list)

    p.y_range.start = 0
    p.x_range.range_padding = 0.1
    p.xaxis.major_label_orientation = 1
    p.xgrid.grid_line_color = None

    return p



module = Blueprint('calories_log', __name__, url_prefix='/')


@module.route('/calories_charts')
@login_required
def calories_charts():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    if not user:
        return redirect(url_for('user.initial_user'))
    week_pie = make_pie_per_week()
    month_pie = make_pie_per_month()
    week_script, week_div = components(week_pie)
    month_script, month_div = components(month_pie) 
    return render_template('calories_log/calories_charts.html',
                           week_div = week_div,
                           week_script=week_script,
                           month_div=month_div,
                           month_script=month_script,
                           user=user)