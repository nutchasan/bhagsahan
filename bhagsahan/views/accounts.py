from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask)
from flask_login import logout_user, current_user, login_required
from .. import models
import datetime
from bhagsahan.forms import LoginForm


module = Blueprint('accounts', __name__)


@module.route('/login', methods=["GET", "POST"])
def login():
    if current_user.is_authenticated is True:
        return redirect(url_for('main.index'))
    # form = LoginForm()
    # if not form.validate_on_submit():
    #     return render_template('accounts/login.html',
    #                            form=form)
    # check_user = models.User.objects(email=form.email.data).first()
    # if check_user:
    #     if current_app.user_manager.verify_password(form.password.data,
    #                                                 check_user['password']):
    #         login_user(check_user)
    #         return redirect(url_for('main.index'))

    return render_template('accounts/login.html',
                            # form=form,
                            invalid_password_error="true")


@module.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('accounts.login'))
