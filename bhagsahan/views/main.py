from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask)
from .. import models
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Flask, redirect, url_for, session, request, jsonify
from authlib.client import OAuth2Session
from flask.json import jsonify
from flask_login import logout_user, current_user, login_required

import datetime
import os


module = Blueprint('main', __name__, url_prefix='/')


@module.route('/')
@login_required
def index():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    
    if not user.weight:
        return redirect(url_for('user.initial_user'))
    if not user.food_group:
        food_group = models.UserFoodGroup()
        user.food_group = food_group
        user.save()

    if (user.today_date.strftime('%Y-%m-%d') != datetime.datetime.now().strftime('%Y-%m-%d')):
        if(user.cal_limit <= 0):
            user.cal_limit_over = True
        else:
            user.cal_limit_over = False
        user.cal_limit = 2000
        user.calories_in = 0
        user.today_date = datetime.datetime.now()

        if user.food_group:
            user.food_group.protein = False
            user.food_group.carbohydrate = False
            user.food_group.vitamin = False
            user.food_group.mineral = False
            user.food_group.fat = False
        else:
            food_group = models.FoodGroup()
            user.food_group = food_group

        if (user.today_date.strftime('%Y-%m-%d') == user.end_week_date.strftime('%Y-%m-%d')):
            user.end_week_date = user.today_date + datetime.timedelta(days=7)
            user.cal_limit_per_week = 14000

        user.save()
        
    if user.encoded_id:
        return redirect(url_for('oauth.get_fitbit'))
    return redirect(url_for('main.dashboard'))



@module.route('/dashboard')
@login_required
def dashboard():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    return render_template('dashboard/dashboard.html',
                           user=user)
