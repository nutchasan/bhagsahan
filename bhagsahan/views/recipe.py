from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask,
                   Response,
                   send_file)
from .. import models
from bhagsahan.forms import RecipeForm, ListForm, ImageRecipeForm
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Flask, redirect, url_for, session, request, jsonify
from flask_login import logout_user, current_user, login_required


import datetime


module = Blueprint('recipe', __name__, url_prefix='/')



@module.route('/recipe')
@login_required
def recipe():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    recipes = models.Recipe.objects()
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('recipe/recipe.html',
                           recipes=recipes,
                           user=user)
    
    
@module.route('/recipe_info/<recipe_id>')
@login_required
def recipe_info(recipe_id):
    user = models.User.objects.get(id=current_user._get_current_object().id)
    recipe = models.Recipe.objects().get(id=recipe_id)
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('recipe/recipe_info.html',
                           recipe=recipe,
                           user=user)


@module.route('/add_recipe', methods=['GET', 'POST'])
@login_required
def add_recipe():
    form = RecipeForm()
    user = models.User.objects.get(id=current_user._get_current_object().id)
    if not user:
        return redirect(url_for('user.initial_user'))
    if not form.validate_on_submit():
        return render_template('recipe/add_recipe.html',
                               form=form,
                               user=user)
    if not form.description.data:
        form.description.data = '-'
    
    return redirect(url_for('recipe.add_list',
                            name=form.name.data,
                            description=form.description.data,
                            tags=form.tags.data,
                            calories=form.calories.data,
                            unit=form.unit.data,
                            ingredient_num=form.ingredient_num.data,
                            methods_num=form.methods_num.data,
                            food_type=form.food_type.data))


@module.route('/add_recipe/add_list/<name>/<description>/<tags>/<int:calories>/<unit>/<int:ingredient_num>/<int:methods_num>/<food_type>',
              methods=['GET', 'POST'])
@login_required
def add_list(name,
             description,
             tags,
             calories,
             unit,
             ingredient_num,
             methods_num,
             food_type):
    user = models.User.objects.get(id=current_user._get_current_object().id)
    form = ListForm()

    for i in range(len(form.ingredients), ingredient_num):
        form.ingredients.append_entry()
    
    for i in range(len(form.methods), methods_num):
        form.methods.append_entry()

    if not user:
        return redirect(url_for('user.initial_user'))
    if not form.validate_on_submit():
        return render_template('recipe/add_list.html',
                               form=form,
                               user=user)
    
    recipe = models.Recipe()
    

    new_srt = tags.strip()
    if new_srt.endswith(','):
        new_srt = new_srt[:-1]
    if new_srt.startswith(','):
        new_srt = new_srt[1:]
    ltags = [tag for tag in  new_srt.split(',') if tag != '']

    new_ingretients = []
    for ingredients_entry in form.ingredients.entries:
        ingredient = models.Ingredient()
        ingredient.name = ingredients_entry.in_name.data
        ingredient.volume = ingredients_entry.volume.data
        ingredient.amount_type = ingredients_entry.amount_type.data
        # recipe.ingredients.append(ingredient)
        new_ingretients.append(ingredient)

    for methods_entry in form.methods.entries:
        recipe.methods.append(methods_entry.info.data)

    recipe.name = name
    recipe.description = description
    recipe.tags = ltags
    recipe.calories = calories
    recipe.unit = unit
    recipe.ingredients = new_ingretients
    if food_type == 'Side dish':
        recipe.side_dish = True    
    recipe.created_date = datetime.datetime.now()
    recipe.update_date = datetime.datetime.now()
    recipe.save()
    
    # item = models.Item(name=name,
    #                    description=description,
    #                    tags=ltags,
    #                    created_date=datetime.datetime.now(),
    #                    updated_date=datetime.datetime.now())
    # item.save()
    
    # nutrition_fact = models.NutritionFact(calories=calories)
    
    # nutrition = models.Nutrition(item=item,
    #                              facts=nutrition_fact,
    #                              created_date=datetime.datetime.now(),
    #                              updated_date=datetime.datetime.now())
    # nutrition.save()

    return redirect(url_for('recipe.add_image_recipe',
                            recipe_id=recipe.id))

@module.route('/edit_recipe/<recipe_id>', methods=['GET', 'POST'])
@login_required
def edit_recipe(recipe_id):
    form = RecipeForm()
    user = models.User.objects.get(id=current_user._get_current_object().id)
    recipe = models.Recipe.objects().get(id=recipe_id)
    if not user:
        return redirect(url_for('user.initial_user'))

    if not form.validate_on_submit():
        form.name.data = recipe.name
        form.description.data = recipe.description
        form.tags.data = ','.join(recipe.tags)
        form.calories.data = recipe.calories
        form.unit.data = recipe.unit
        form.ingredient_num.data = len(recipe.ingredients)
        form.methods_num.data = len(recipe.methods)
        if recipe.side_dish:
            form.food_type.data = 'Side dish'

        
        return render_template('recipe/edit_recipe.html',
                               form=form,
                               recipe=recipe,
                               user=user)
    
    return redirect(url_for('recipe.edit_list',
                            recipe_id=recipe_id,
                            name=form.name.data,
                            description=form.description.data,
                            tags=form.tags.data,
                            calories=form.calories.data,
                            unit=form.unit.data,
                            ingredient_num=form.ingredient_num.data,
                            methods_num=form.methods_num.data,
                            food_type=form.food_type.data))




@module.route('/edit_recipe/add_list/<recipe_id>/<name>/<description>/<tags>/<int:calories>/<unit>/<int:ingredient_num>/<int:methods_num>/<food_type>',
              methods=['GET', 'POST'])
@login_required
def edit_list(recipe_id,
              name,
              description,
              tags,
              calories,
              unit,
              ingredient_num,
              methods_num,
              food_type):
    user = models.User.objects.get(id=current_user._get_current_object().id)
    form = ListForm()
    recipe = models.Recipe.objects().get(id=recipe_id)

    for i in range(len(form.ingredients), ingredient_num):
        form.ingredients.append_entry()
    
    for i in range(len(form.methods), methods_num):
        form.methods.append_entry()            

    if not user:
        return redirect(url_for('user.initial_user'))
    if not form.validate_on_submit():
        for ingredients_entry in form.ingredients.entries:
            if len(recipe.ingredients) > form.ingredients.entries.index(ingredients_entry):
                ingredients_entry.in_name.data = recipe.ingredients[form.ingredients.entries.index(ingredients_entry)].name
                ingredients_entry.volume.data = recipe.ingredients[form.ingredients.entries.index(ingredients_entry)].volume
                ingredients_entry.amount_type.data = recipe.ingredients[form.ingredients.entries.index(ingredients_entry)].amount_type

        for methods_entry in form.methods.entries:
            if len(recipe.methods) > form.methods.entries.index(methods_entry):
                methods_entry.info.data = recipe.methods[form.methods.entries.index(methods_entry)]
        
        return render_template('recipe/edit_list.html',
                               form=form,
                               user=user)

    new_srt = tags.strip()
    if new_srt.endswith(','):
        new_srt = new_srt[:-1]
    if new_srt.startswith(','):
        new_srt = new_srt[1:]
    ltags = [tag for tag in  new_srt.split(',') if tag != '']

    new_ingredient = []
    for ingredients_entry in form.ingredients.entries:
        ingredient = models.Ingredient()
        ingredient.name = ingredients_entry.in_name.data
        ingredient.volume = ingredients_entry.volume.data
        ingredient.amount_type = ingredients_entry.amount_type.data
        new_ingredient.append(ingredient)

    update_methods = []
    for methods_entry in form.methods.entries:
        update_methods.append(methods_entry.info.data)
    
    for i in new_ingredient:
        print(i.name)
        
    recipe.name = name
    recipe.description = description
    recipe.tags = ltags
    recipe.calories = calories
    recipe.unit = unit
    recipe.ingredients = new_ingredient
    recipe.methods = update_methods
    if food_type == 'Side dish':
        recipe.side_dish = True
    recipe.created_date = datetime.datetime.now()
    recipe.update_date = datetime.datetime.now()
    recipe.save()

    return redirect(url_for('recipe.edit_image_recipe',
                            recipe_id=recipe_id))
    
    
@module.route('/add_image_recipe/<recipe_id>', methods=['GET', 'POST'])
@login_required
def add_image_recipe(recipe_id):
    form = ImageRecipeForm()
    user = models.User.objects.get(id=current_user._get_current_object().id)
    recipe = models.Recipe.objects().get(id=recipe_id)
    if not user:
        return redirect(url_for('user.initial_user'))

    if not form.validate_on_submit():
        return render_template('recipe/add_image.html',
                               form=form,
                               recipe=recipe,
                               user=user)
    if form.image.data:
        recipe.image.put(form.image.data,
                            filename=form.image.data.filename)
        recipe.save()

    return redirect(url_for('recipe.recipe_info',
                            recipe_id=recipe_id))
    
    
@module.route('/edit_image_recipe/<recipe_id>', methods=['GET', 'POST'])
@login_required
def edit_image_recipe(recipe_id):
    form = ImageRecipeForm()
    user = models.User.objects.get(id=current_user._get_current_object().id)
    recipe = models.Recipe.objects().get(id=recipe_id)
    if not user:
        return redirect(url_for('user.initial_user'))

    if not form.validate_on_submit():
        return render_template('recipe/edit_image.html',
                               form=form,
                               recipe=recipe,
                               user=user)
    if form.image.data:
        recipe.image.replace(form.image.data,
                            filename=form.image.data.filename)
        recipe.save()

    return redirect(url_for('recipe.recipe_info',
                            recipe_id=recipe_id))


@module.route('/delete_recipe/<recipe_id>', methods=['GET', 'POST'])
def delete_recipe(recipe_id):
    recipe = models.Recipe.objects.get(id=recipe_id)
    recipe.delete()
    
    return redirect(url_for('recipe.recipe'))


@module.route('recipe_image/<recipe_id>/<file_name>', methods=["GET", "POST"])
def recipe_image(recipe_id, file_name):
    recipe = models.Recipe.objects.get(id=recipe_id)
    img = None
    if recipe.image.filename == file_name:
        img = recipe.image
    response = Response()
    if img:
        response = send_file(img, mimetype='image/jpg')
    else:
        response.status_code = 404

    return response