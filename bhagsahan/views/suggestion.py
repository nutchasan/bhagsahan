from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask)
from mongoengine.queryset.visitor import Q
from .. import models
from bhagsahan.forms import SelectMoreFoodForm, NumberOfPelpleForm
# from the-warehourse import oauth2
# from the-warehourse.forms import LoginForm, RegisterForm
# from flask_user import (UserMixin, 
#                         login_required, 
#                         current_user,
#                         user_manager)
# from flask_login import login_user
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Flask, redirect, url_for, session, request, jsonify
from flask_login import logout_user, current_user, login_required

import datetime


module = Blueprint('suggestion', __name__, url_prefix='/')


def update_food_status():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    recipes = models.Recipe.objects()
    stocks = models.Stock.objects() 

    suggested_recipes = []
    un_suggested_recipes = []

    for recipe in recipes:
        check_list = []
        recipe_status_list = []
        recipe_allergen_list = []
        for recipe_ingredient in recipe.ingredients:
            check = False
            recipe_status = False
            recipe_allergen = False
            for stock in stocks:
                if recipe_ingredient.name == stock.name:
                    check = True
                    if stock.volume >= recipe_ingredient.volume:
                        recipe_status = True
                    if stock.nutrition and user.allergen and stock.nutrition.allergen:
                        if (stock.nutrition.allergen.eggs and user.allergen.eggs) or \
                           (stock.nutrition.allergen.fish and user.allergen.fish) or \
                           (stock.nutrition.allergen.gluten and user.allergen.gluten) or \
                           (stock.nutrition.allergen.milk and user.allergen.milk) or \
                           (stock.nutrition.allergen.peanuts and user.allergen.peanuts) or \
                           (stock.nutrition.allergen.shellfish and user.allergen.shellfish) or \
                           (stock.nutrition.allergen.soybeans and user.allergen.soybeans) or \
                           (stock.nutrition.allergen.tree_nuts and user.allergen.tree_nuts) or \
                           (stock.nutrition.allergen.wheat and user.allergen.wheat):
                            recipe_allergen = True

            check_list.append(check)
            recipe_status_list.append(recipe_status)
            recipe_allergen_list.append(recipe_allergen)

        recipe.ingredient_status = True           
        for i in check_list:
            if i == False:
                recipe.ingredient_status = False

        recipe.status = True
        for i in recipe_status_list:
            if i == False:
                recipe.status = False

        recipe.allergen_status = False
        for i in recipe_allergen_list:
            if i == True:
                recipe.allergen_status = True         

        recipe.save()


@module.route('/suggest', methods=['GET', 'POST'])
@login_required
def suggest_menu():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    recipes = models.Recipe.objects()
    stocks = models.Stock.objects() 
    if not user:
        return redirect(url_for('user.initial_user'))

    update_food_status()

    return render_template('suggestion/suggest.html',
                            user=user,
                            recipes=recipes)
        

@module.route('/suggest_a_la_carte', methods=['GET', 'POST'])
@login_required
def suggest_menu_a_la_carte():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    update_food_status()
    recipes = models.Recipe.objects()
    stocks = models.Stock.objects() 
    if not user:
        return redirect(url_for('user.initial_user'))

    return render_template('suggestion/suggest_a_la_carte.html',
                            user=user,
                            recipes=recipes)
        

@module.route('/suggest_side_dish', methods=['GET', 'POST'])
@login_required
def suggest_menu_side_dish():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    update_food_status()
    recipes = models.Recipe.objects(side_dish=True)
    stocks = models.Stock.objects() 
    if not user:
        return redirect(url_for('user.initial_user'))

    return render_template('suggestion/suggest_side_dish.html',
                            user=user,
                            recipes=recipes)


@module.route('/rec_suggest', methods=['GET', 'POST'])
@login_required
def rec_suggest():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    update_food_status()
    recipes = models.Recipe.objects()
    stocks = models.Stock.objects() 
    form = SelectMoreFoodForm()
    if not user:
        return redirect(url_for('user.initial_user'))

    return render_template('suggestion/rec_suggest.html',
                            user=user,
                            recipes=recipes)
    

@module.route('/rec_suggest_a_la_carte', methods=['GET', 'POST'])
@login_required
def rec_suggest_a_la_carte():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    update_food_status()
    recipes = models.Recipe.objects()
    stocks = models.Stock.objects() 
    form = SelectMoreFoodForm()
    if not user:
        return redirect(url_for('user.initial_user'))
    
    return render_template('suggestion/rec_suggest_a_la_carte.html',
                            user=user,
                            recipes=recipes)
    

@module.route('/rec_suggest_side_dish', methods=['GET', 'POST'])
@login_required
def rec_suggest_side_dish():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    update_food_status()
    recipes = models.Recipe.objects(side_dish=True)
    stocks = models.Stock.objects() 
    form = SelectMoreFoodForm()
    if not user:
        return redirect(url_for('user.initial_user'))

    return render_template('suggestion/rec_suggest_side_dish.html',
                            user=user,
                            recipes=recipes)


@module.route('/order_suggest', methods=['GET', 'POST'])
@login_required
def order_suggest():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    update_food_status()
    recipes = models.Recipe.objects()
    stocks = models.Stock.objects() 
    form = SelectMoreFoodForm()
    if not user:
        return redirect(url_for('user.initial_user'))
    
    return render_template('suggestion/order_suggest.html',
                            user=user,
                            recipes=recipes)


@module.route('/order_suggest_a_la_carte', methods=['GET', 'POST'])
@login_required
def order_suggest_a_la_carte():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    update_food_status()
    recipes = models.Recipe.objects.order_by('calories')
    stocks = models.Stock.objects() 
    form = SelectMoreFoodForm()
    if not user:
        return redirect(url_for('user.initial_user'))

    return render_template('suggestion/order_suggest_a_la_carte.html',
                            user=user,
                            recipes=recipes)


@module.route('/order_suggest_side_dish', methods=['GET', 'POST'])
@login_required
def order_suggest_side_dish():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    update_food_status()
    recipes = models.Recipe.objects(side_dish=True).order_by('calories')
    stocks = models.Stock.objects() 
    form = SelectMoreFoodForm()
    if not user:
        return redirect(url_for('user.initial_user'))

    return render_template('suggestion/order_suggest_side_dish.html',
                            user=user,
                            recipes=recipes)


@module.route('/suggest_info/<recipe_id>')
@login_required
def suggest_info(recipe_id):
    user = models.User.objects.get(id=current_user._get_current_object().id)
    recipe = models.Recipe.objects().get(id=recipe_id)
    stocks = models.Stock.objects()
    list_stock_name=[]
    for stock in stocks:
        list_stock_name.append(stock.name)

    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('suggestion/suggest_info.html',
                           recipe=recipe,
                           stocks=stocks,
                           list_stock_name=list_stock_name,
                           user=user)


@module.route('/food_selected/<int:cal>/<recipe_id>', methods=['GET', 'POST'])
@login_required
def food_selected(cal, recipe_id):
    user = models.User.objects.get(id=current_user._get_current_object().id)
    recipe = models.Recipe.objects().get(id=recipe_id)
    stocks = models.Stock.objects()
    if not user:
        return redirect(url_for('user.initial_user'))

    user.cal_limit -= cal
    user.cal_limit_per_week -= cal
    user.calories_in += cal
    if user.cal_limit < 0:
        user.cal_limit = 0
    if user.cal_limit_per_week < 0:
        user.cal_limit_per_week = 0
        
    for ingredient in recipe.ingredients:
        for stock in stocks:
            if ingredient.name == stock.name:
                stock.volume -= ingredient.volume
                if stock.volume < 0:
                    stock.volume = 0
                stock.save()    
                break
        for stock in stocks:
            if ingredient.name == stock.name and stock.nutrition.food_group:
                if stock.nutrition.food_group.protein:
                    user.food_group.protein = True
                if stock.nutrition.food_group.carbohydrate:
                    user.food_group.carbohydrate = True
                if stock.nutrition.food_group.vitamin:
                    user.food_group.vitamin = True
                if stock.nutrition.food_group.mineral:
                    user.food_group.mineral = True
                if stock.nutrition.food_group.fat:
                    user.food_group.fat = True
    
    user.save()
    
    eating_log = models.EatingLog(user=user,
                                  food_name=recipe.name,
                                  calories=recipe.calories,
                                  category='Home',
                                  recipe_infomation=recipe,
                                  created_date=datetime.datetime.now(),
                                  update_date=datetime.datetime.now())
     
    eating_log.datetime_eat = datetime.datetime.now()
    eating_log.save()
    
    return redirect(url_for('recipe.recipe_info',
                            recipe_id=recipe_id))
    

@module.route('/food_selected_side_dish/<int:cal>/<recipe_id>', methods=['GET', 'POST'])
@login_required
def food_selected_side_dish(cal, recipe_id):
    user = models.User.objects.get(id=current_user._get_current_object().id)
    form = NumberOfPelpleForm()
    recipe = models.Recipe.objects().get(id=recipe_id)
    stocks = models.Stock.objects()
    if not user:
        return redirect(url_for('user.initial_user'))

    if not form.validate_on_submit():
        return render_template('suggestion/num_people.html',
                               form=form,
                               user=user)

    cal = cal / form.num_people.data
    user.cal_limit -= cal
    user.cal_limit_per_week -= cal
    user.calories_in += cal
    if user.cal_limit < 0:
        user.cal_limit = 0
    if user.cal_limit_per_week < 0:
        user.cal_limit_per_week = 0

    for ingredient in recipe.ingredients:
        for stock in stocks:
            if ingredient.name == stock.name:
                stock.volume -= ingredient.volume
                if stock.volume < 0:
                    stock.volume = 0
                stock.save()    
                break
        for stock in stocks:
            if ingredient.name == stock.name and stock.nutrition.food_group:
                if stock.nutrition.food_group.protein:
                    user.food_group.protein = True
                if stock.nutrition.food_group.carbohydrate:
                    user.food_group.carbohydrate = True
                if stock.nutrition.food_group.vitamin:
                    user.food_group.vitamin = True
                if stock.nutrition.food_group.mineral:
                    user.food_group.mineral = True
                if stock.nutrition.food_group.fat:
                    user.food_group.fat = True
    
    user.save()


    eating_log = models.EatingLog(user=user,
                                  food_name=recipe.name,
                                  calories=recipe.calories,
                                  category='Home',
                                  recipe_infomation=recipe,
                                  created_date=datetime.datetime.now(),
                                  update_date=datetime.datetime.now())
     
    eating_log.datetime_eat = datetime.datetime.now()
    eating_log.save()

    return redirect(url_for('recipe.recipe_info',
                            recipe_id=recipe_id))



@module.route('/suggestion_search_result', methods=["GET"])
@login_required
def suggestion_search_result():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    search = request.args.get('search')
    update_food_status()
    recipes = models.Recipe.objects((Q(name__icontains=search) |
                                Q(description__icontains=search) |
                                Q(tags__icontains=search)))

    return render_template('/suggestion/search_suggestion.html',
                           user=user,
                           recipes=recipes)


@module.route('/update_data', methods=['GET', 'POST'])
@login_required
def update_data():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    if not user:
        return redirect(url_for('user.initial_user'))
    user.cal_limit = 2000
    user.cal_limit_per_week = 14000
    user.cal_limit_over = False
    user.end_week_date = datetime.datetime.now() + datetime.timedelta(days=7)
    user.calories_in = 0
    user.save()
    return redirect(url_for('oauth.get_fitbit'))


