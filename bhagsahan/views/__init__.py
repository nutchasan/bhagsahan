from flask import Flask
from . import (main,
               user,
               suggestion,
               food_log,
               oauth,
               stock,
               recipe,
               accounts,
               eating_log,
               food,
               calories_log)


def register_blueprint(app):
    app.register_blueprint(main.module)
    app.register_blueprint(user.module)
    app.register_blueprint(suggestion.module)
    app.register_blueprint(food_log.module)
    app.register_blueprint(oauth.module)
    app.register_blueprint(stock.module)
    app.register_blueprint(recipe.module)
    app.register_blueprint(accounts.module)
    app.register_blueprint(eating_log.module)
    app.register_blueprint(food.module)
    app.register_blueprint(calories_log.module)
    

