from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask,
                   Response,
                   send_file)
from .. import models
from bhagsahan.forms import FoodLogForm, UPCForm
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Flask, redirect, url_for, session, request, jsonify
from flask_login import logout_user, current_user, login_required


import datetime
import requests
import json

module = Blueprint('food_log', __name__, url_prefix='/')
BASE_URL = 'https://api.upcitemdb.com/prod/trial'



class UPCItemDBClient:
    def __init__(self):
        pass

    def lookup(self, upc):
        url = '/lookup'
        params = {'upc': upc}
        response = requests.get(BASE_URL + url, params=params)
        return response.json()



@module.route('/food_log')
@login_required
def food_log():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    nutritions = models.Nutrition.objects() 
    items = models.Item.objects()
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('food_log/food_log.html',
                            nutritions=nutritions,
                            items=items,user=user)


@module.route('/nutrition/<nutrition_id>', methods=['GET', 'POST'])
@login_required
def nutrition(nutrition_id):
    user = models.User.objects.get(id=current_user._get_current_object().id)
    nutrition = models.Nutrition.objects().get(id=nutrition_id) 
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('food_log/nutrition.html',
                            nutrition=nutrition,
                            user=user)


@module.route('/add_upc', methods=['GET', 'POST'])
@login_required
def add_upc():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    form = UPCForm()
    
    if not user:
        return redirect(url_for('user.initial_user'))
    if not form.validate_on_submit():
        return render_template('food_log/add_upc.html',
                            form=form,
                            user=user)

    items = models.Item.objects()
    for item in items:
        if item.upc == form.upc.data:
            nutrition = models.Nutrition.objects().get(item=item)
            return redirect(url_for('food_log.nutrition',
                                    nutrition_id=nutrition.id))
    look_item = UPCItemDBClient()
    result = look_item.lookup(form.upc.data) 
    if result['code'] != 'OK':
        return redirect(url_for('food_log.add_food_log'))
    
    data = look_item.lookup(form.upc.data)
    print_data = json.dumps(data, indent=4)
    print(print_data)

    new_item = models.Item(name=data['items'][0]['title'],
                           description=data['items'][0]['description'],
                           upc=data['items'][0]['upc'],
                           color=data['items'][0]['color'],
                           size=data['items'][0]['size'],
                           weight=data['items'][0]['weight'],
                           dimension=data['items'][0]['dimension'],
                           created_date=datetime.datetime.now(),
                           updated_date=datetime.datetime.now())
    new_item.save()
    new_allergen = models.Allergen()
    new_facts = models.NutritionFact()
    new_nutrition = models.Nutrition(item=new_item,
                                     allergen=new_allergen,
                                     facts=new_facts,
                                     created_date=datetime.datetime.now(),
                                     updated_date=datetime.datetime.now())
    new_nutrition.save()
    return redirect(url_for('food_log.nutrition',
                            nutrition_id=new_nutrition.id))
    # return 'eiee'
        


@module.route('/add_food_log', methods=['GET', 'POST'])
@login_required
def add_food_log():
    form = FoodLogForm()
    user = models.User.objects.get(id=current_user._get_current_object().id)
    if not user:
        return redirect(url_for('user.initial_user'))
    if not form.validate_on_submit():
        return render_template('food_log/add_food_log.html',
                               form=form,
                               user=user)

    new_srt = form.tags.data.strip()
    if new_srt.endswith(','):
        new_srt = new_srt[:-1]
    if new_srt.startswith(','):
        new_srt = new_srt[1:]
    ltags = [tag for tag in  new_srt.split(',') if tag != '']

    item = models.Item(name=form.name.data,
                       description=form.description.data,
                       tags=ltags,
                       upc=form.upc.data,
                       color=form.color.data,
                       size=form.size.data,
                       weight=form.weight.data,
                       dimension=form.dimension.data,
                       status=form.status.data,
                       created_date=datetime.datetime.now(),
                       updated_date=datetime.datetime.now())
    
    if form.image.data:
        item.image.put(form.image.data,
                                filename=form.image.data.filename)
    if form.nutrition_image.data:
        item.nutrition_image.put(form.nutrition_image.data,
                                filename=form.nutrition_image.data.filename)
    if form.ingredient_image.data:
        item.ingredient_image.put(form.ingredient_image.data,
                                filename=form.ingredient_image.data.filename)
    if form.upc_image.data:
        item.upc_image.put(form.upc_image.data,
                                filename=form.upc_image.data.filename)
    item.save()    

    allergen = models.Allergen(eggs=form.eggs.data,
                               fish=form.fish.data,
                               gluten=form.gluten.data,
                               milk=form.milk.data,
                               peanuts=form.peanuts.data,
                               shellfish=form.shellfish.data,  
                               soybeans=form.soybeans.data,
                               tree_nuts=form.tree_nuts.data,
                               wheat=form.wheat.data)

    facts = models.NutritionFact(calcium_dv=form.calcium_dv.data,
                                 calories=form.calories.data,
                                 calories_from_fat=form.calories_from_fat.data,
                                 cholesterol=form.cholesterol.data,
                                 dietary_fiber=form.dietary_fiber.data,
                                 ingredient_statement=form.ingredient_statement.data,
                                 iron_dv=form.iron_dv.data,
                                 monounsaturated_fat=form.monounsaturated_fat.data,
                                 polyunsaturated_fat=form.polyunsaturated_fat.data,
                                 protein=form.proteins.data,
                                 refuse_pct=form.refuse_pct.data,
                                 saturated_fat=form.saturated_fat.data,
                                 serving_size_quantity=form.serving_size_quantity.data,
                                 serving_size_unit=form.serving_size_unit.data,
                                 serving_weight_grams=form.serving_weight_grams.data,
                                 servings_per_container=form.servings_per_container.data,
                                 sodium=form.sodium.data,
                                 sugars=form.sugars.data,
                                 total_carbohydrate=form.total_carbohydrate.data,
                                 total_fat=form.total_fat.data,
                                 trans_fatty_acid=form.trans_fatty_acid.data,
                                 vitamin_a_dv=form.vitamin_a_dv.data,
                                 vitamin_c_dv=form.vitamin_c_dv.data,
                                 water_grams=form.water_grams.data)
    
    food_group = models.FoodGroup(protein=form.protein.data,
                                  carbohydrate=form.carbohydrate.data,
                                  vitamin=form.vitamin.data,
                                  mineral=form.mineral.data,
                                  fat=form.fat.data)

    nutrition = models.Nutrition(item=item,
                                 allergen=allergen,
                                 facts=facts,
                                 food_group=food_group,
                                 created_date=datetime.datetime.now(),
                                 updated_date=datetime.datetime.now())

    nutrition.save()

    return redirect(url_for('food_log.nutrition',
                            nutrition_id=nutrition.id))



@module.route('/edit_food_log/<item_id>/<nutrition_id>', methods=['GET', 'POST'])
@login_required
def edit_food_log(item_id, nutrition_id):
    form = FoodLogForm()
    user = models.User.objects.get(id=current_user._get_current_object().id)
    item = models.Item.objects.get(id=item_id)
    nutrition = models.Nutrition.objects.get(id=nutrition_id)

    if not user:
        return redirect(url_for('user.initial_user'))

    if not form.validate_on_submit():
        form.name.data = item.name
        form.description.data = item.description
        form.tags.data = ','.join(item.tags)
        form.upc.data = item.upc
        form.color.data = item.color
        form.size.data = item.size
        form.weight.data = item.weight
        form.dimension.data = item.dimension

        form.eggs.data = nutrition.allergen.eggs
        form.fish.data = nutrition.allergen.fish
        form.gluten.data = nutrition.allergen.gluten
        form.milk.data = nutrition.allergen.milk
        form.peanuts.data = nutrition.allergen.peanuts
        form.shellfish.data = nutrition.allergen.shellfish
        form.soybeans.data = nutrition.allergen.soybeans
        form.tree_nuts.data = nutrition.allergen.tree_nuts
        form.wheat.data = nutrition.allergen.wheat

        if not nutrition.food_group:
            form.protein.data = False
            form.carbohydrate.data = False
            form.vitamin.data = False
            form.mineral.data = False
            form.fat.data = False
        else:
            form.protein.data = nutrition.food_group.protein
            form.carbohydrate.data = nutrition.food_group.carbohydrate
            form.vitamin.data = nutrition.food_group.vitamin
            form.mineral.data = nutrition.food_group.mineral
            form.fat.data = nutrition.food_group.fat
            

        form.calcium_dv.data = nutrition.facts.calcium_dv
        form.calories.data = nutrition.facts.calories
        form.calories_from_fat.data = nutrition.facts.calories_from_fat
        form.cholesterol.data = nutrition.facts.cholesterol
        form.dietary_fiber.data = nutrition.facts.dietary_fiber
        form.ingredient_statement.data = nutrition.facts.ingredient_statement
        form.iron_dv.data = nutrition.facts.iron_dv
        form.monounsaturated_fat.data = nutrition.facts.monounsaturated_fat
        form.polyunsaturated_fat.data = nutrition.facts.polyunsaturated_fat
        form.proteins.data = nutrition.facts.protein
        form.refuse_pct.data = nutrition.facts.refuse_pct
        form.saturated_fat.data = nutrition.facts.saturated_fat
        form.serving_size_quantity.data = nutrition.facts.serving_size_quantity
        form.serving_size_unit.data = nutrition.facts.serving_size_unit
        form.serving_weight_grams.data = nutrition.facts.serving_weight_grams
        form.servings_per_container.data = nutrition.facts.servings_per_container
        form.sodium.data = nutrition.facts.sodium
        form.sugars.data = nutrition.facts.sugars
        form.total_carbohydrate.data = nutrition.facts.total_carbohydrate
        form.total_fat.data = nutrition.facts.total_fat
        form.trans_fatty_acid.data = nutrition.facts.trans_fatty_acid
        form.vitamin_a_dv.data = nutrition.facts.vitamin_a_dv
        form.vitamin_c_dv.data = nutrition.facts.vitamin_c_dv
        form.water_grams.data = nutrition.facts.water_grams
        return render_template('food_log/edit_food_log.html',
                               form=form,
                               nutrition=nutrition,
                               user=user)

    new_srt = form.tags.data.strip()
    if new_srt.endswith(','):
        new_srt = new_srt[:-1]
    if new_srt.startswith(','):
        new_srt = new_srt[1:]
    ltags = [tag for tag in  new_srt.split(',') if tag != '']

    
    item.name = form.name.data
    item.description = form.description.data
    item.tags = ltags
    item.upc = form.upc.data
    item.color = form.color.data
    item.size = form.size.data
    item.weight = form.weight.data
    item.dimension = form.dimension.data
    item.status = form.status.data
    item.updated_date = datetime.datetime.now()
    

    if form.image.data:
        item.image.replace(form.image.data,
                            filename=form.image.data.filename)
    if form.nutrition_image.data:
        item.nutrition_image.put(form.nutrition_image.data,
                                filename=form.nutrition_image.data.filename)
    if form.ingredient_image.data:
        item.ingredient_image.put(form.ingredient_image.data,
                                filename=form.ingredient_image.data.filename)
    if form.upc_image.data:
        item.upc_image.put(form.upc_image.data,
                                filename=form.upc_image.data.filename)
    item.save()    

    allergen = models.Allergen(eggs=form.eggs.data,
                               fish=form.fish.data,
                               gluten=form.gluten.data,
                               milk=form.milk.data,
                               peanuts=form.peanuts.data,
                               shellfish=form.shellfish.data,  
                               soybeans=form.soybeans.data,
                               tree_nuts=form.tree_nuts.data,
                               wheat=form.wheat.data)

    facts = models.NutritionFact(calcium_dv=form.calcium_dv.data,
                                 calories=form.calories.data,
                                 calories_from_fat=form.calories_from_fat.data,
                                 cholesterol=form.cholesterol.data,
                                 dietary_fiber=form.dietary_fiber.data,
                                 ingredient_statement=form.ingredient_statement.data,
                                 iron_dv=form.iron_dv.data,
                                 monounsaturated_fat=form.monounsaturated_fat.data,
                                 polyunsaturated_fat=form.polyunsaturated_fat.data,
                                 protein=form.protein.data,
                                 refuse_pct=form.refuse_pct.data,
                                 saturated_fat=form.saturated_fat.data,
                                 serving_size_quantity=form.serving_size_quantity.data,
                                 serving_size_unit=form.serving_size_unit.data,
                                 serving_weight_grams=form.serving_weight_grams.data,
                                 servings_per_container=form.servings_per_container.data,
                                 sodium=form.sodium.data,
                                 sugars=form.sugars.data,
                                 total_carbohydrate=form.total_carbohydrate.data,
                                 total_fat=form.total_fat.data,
                                 trans_fatty_acid=form.trans_fatty_acid.data,
                                 vitamin_a_dv=form.vitamin_a_dv.data,
                                 vitamin_c_dv=form.vitamin_c_dv.data,
                                 water_grams=form.water_grams.data)
    
    food_group = models.FoodGroup(protein=form.protein.data,
                                  carbohydrate=form.carbohydrate.data,
                                  vitamin=form.vitamin.data,
                                  mineral=form.mineral.data,
                                  fat=form.fat.data)

    nutrition.item = item
    nutrition.allergen = allergen
    nutrition.facts = facts
    nutrition.food_group = food_group
    nutrition.updated_date = datetime.datetime.now()
    
    nutrition.save()

    return redirect(url_for('food_log.nutrition',
                            nutrition_id=nutrition.id))


@module.route('/delete_food_log/<item_id>/<nutrition_id>', methods=['GET', 'POST'])
def delete_food_log(item_id, nutrition_id):
    item = models.Item.objects.get(id=item_id)
    nutrition = models.Nutrition.objects.get(id=nutrition_id)
    item.delete()
    nutrition.delete()

    return redirect(url_for('food_log.food_log'))


@module.route('food_log_image/<item_id>/<file_name>', methods=["GET", "POST"])
def food_log_image(item_id, file_name):
    item = models.Item.objects.get(id=item_id)
    img = None
    if item.image.filename == file_name:
        img = item.image
    response = Response()
    if img:
        response = send_file(img, mimetype='image/jpg')
    else:
        response.status_code = 404

    return response


@module.route('food_log_nutrition_image/<item_id>/<file_name>', methods=["GET", "POST"])
def food_log_nutrition_image(item_id, file_name):
    item = models.Item.objects.get(id=item_id)
    img = None
    if item.nutrition_image.filename == file_name:
        img = item.nutrition_image
    response = Response()
    if img:
        response = send_file(img, mimetype='image/jpg')
    else:
        response.status_code = 404

    return response


@module.route('food_log_ingredient_image/<item_id>/<file_name>', methods=["GET", "POST"])
def food_log_ingredient_image(item_id, file_name):
    item = models.Item.objects.get(id=item_id)
    img = None
    if item.ingredient_image.filename == file_name:
        img = item.ingredient_image

    response = Response()
    if img:
        response = send_file(img, mimetype='image/jpg')
    else:
        response.status_code = 404

    return response


@module.route('food_log_upc_image/<item_id>/<file_name>', methods=["GET", "POST"])
def food_log_upc_image(item_id, file_name):
    item = models.Item.objects.get(id=item_id)
    img = None
    if item.upc_image.filename == file_name:
        img = item.upc_image

    response = Response()
    if img:
        response = send_file(img, mimetype='image/jpg')
    else:
        response.status_code = 404

    return response