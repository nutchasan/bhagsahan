from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask,
                   Response,
                   send_file)
from .. import models
from mongoengine.queryset.visitor import Q
from bhagsahan.forms import SelectMoreFoodForm, FoodForm, SetTime
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Flask, redirect, url_for, session, request, jsonify
from flask_login import logout_user, current_user, login_required



import datetime


module = Blueprint('food', __name__, url_prefix='/')


@module.route('/another_food', methods=['GET', 'POST'])
@login_required
def another_food():
    form = SelectMoreFoodForm()
    user = models.User.objects.get(id=current_user._get_current_object().id)

    if not form.validate_on_submit():
        return render_template('food/another_food.html',
                                user=user,
                                form=form)

    if form.cal_in.data > 0:
        user.cal_limit -= form.cal_in.data
        user.cal_limit_per_week -= form.cal_in.data
        user.calories_in += form.cal_in.data
        if user.cal_limit < 0:
            user.cal_limit = 0
        if user.cal_limit_per_week < 0:
            user.cal_limit_per_week = 0
        user.save()
        
        eating_log = models.EatingLog(user=user,
                                      food_name=form.food_name.data,
                                      calories=form.cal_in.data,
                                      category='Food from outside',
                                      created_date=datetime.datetime.now(),
                                      update_date=datetime.datetime.now())
        day = int(form.date_eat.data.strftime('%d'))
        month = int(form.date_eat.data.strftime('%m'))
        year = int(form.date_eat.data.strftime('%Y'))
        hour = int(form.time_eat.data.strftime('%H'))
        minute = int(form.time_eat.data.strftime('%M'))
        eating_log.datetime_eat = datetime.datetime(year, month, day, hour, minute)
        eating_log.save()

        
                                      
        return redirect(url_for('eating_log.eating_list'))



@module.route('/food_list')
@login_required
def food_list():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    food = models.Food.objects()
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('food/food_list.html',
                           food=food,
                           user=user)


@module.route('/food_list_order')
@login_required
def food_list_order():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    food = models.Food.objects().order_by('calories')
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('food/food_list_order.html',
                           food=food,
                           user=user)


@module.route('/food_list_recommend')
@login_required
def food_list_recommend():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    food = models.Food.objects(Q(calories__lte=600))
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('food/food_list_recommend.html',
                           food=food,
                           user=user)


@module.route('/food_information/<food_id>')
@login_required
def food_information(food_id):
    user = models.User.objects.get(id=current_user._get_current_object().id)
    food = models.Food.objects().get(id=food_id)
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('food/food_information.html',
                           food=food,
                           user=user)
    

@module.route('/add_food', methods=['GET', 'POST'])
@login_required
def add_food():
    form = FoodForm()
    user = models.User.objects.get(id=current_user._get_current_object().id)
    if not user:
        return redirect(url_for('user.initial_user'))
    if not form.validate_on_submit():
        return render_template('food/add_food.html',
                               form=form,
                               user=user)
    
    new_srt = form.tags.data.strip()
    if new_srt.endswith(','):
        new_srt = new_srt[:-1]
    if new_srt.startswith(','):
        new_srt = new_srt[1:]
    ltags = [tag for tag in  new_srt.split(',') if tag != '']

    food_group = models.RestaurantFoodGroup(protein=form.protein.data,
                                  carbohydrate=form.carbohydrate.data,
                                  vitamin=form.vitamin.data,
                                  mineral=form.mineral.data,
                                  fat=form.fat.data)
    
    new_food = models.Food(name=form.name.data,
                           description=form.description.data,
                           restaurant=form.restaurant.data,
                           calories=form.calories.data,
                           tags=ltags,
                           food_group=food_group,
                           user_created=user,
                           created_date=datetime.datetime.now(),
                           update_date=datetime.datetime.now())
    if form.image.data:
        new_food.image.put(form.image.data,
                            filename=form.image.data.filename)
    new_food.save()
    
    

    return redirect(url_for('food.food_information',
                            food_id=new_food.id))
    

@module.route('/edit_food/<food_id>', methods=['GET', 'POST'])
@login_required
def edit_food(food_id):
    form = FoodForm()
    user = models.User.objects.get(id=current_user._get_current_object().id)
    food = models.Food.objects.get(id=food_id)
    if not user:
        return redirect(url_for('user.initial_user'))
    if not form.validate_on_submit():
        form.name.data = food.name
        form.description.data = food.description
        form.restaurant.data = food.restaurant
        form.calories.data = food.calories
        form.tags.data = ",".join(food.tags)
        form.image.data = food.image
        
        if not food.food_group:
            form.protein.data = False
            form.carbohydrate.data = False
            form.vitamin.data = False
            form.mineral.data = False
            form.fat.data = False
        else:
            form.protein.data = food.food_group.protein
            form.carbohydrate.data = food.food_group.carbohydrate
            form.vitamin.data = food.food_group.vitamin
            form.mineral.data = food.food_group.mineral
            form.fat.data = food.food_group.fat

        return render_template('food/edit_food.html',
                               form=form,
                               user=user,
                               food=food)
    
    new_srt = form.tags.data.strip()
    if new_srt.endswith(','):
        new_srt = new_srt[:-1]
    if new_srt.startswith(','):
        new_srt = new_srt[1:]
    ltags = [tag for tag in  new_srt.split(',') if tag != '']

    food.name = form.name.data
    food.description = form.description.data
    food.restaurant = form.restaurant.data
    food.calories = form.calories.data
    food.tags = ltags
    if form.image.data:
        food.image.replace(form.image.data,
                           filename=form.image.data.filename)

    food_group = models.RestaurantFoodGroup(protein=form.protein.data,
                                            carbohydrate=form.carbohydrate.data,
                                            vitamin=form.vitamin.data,
                                            mineral=form.mineral.data,
                                            fat=form.fat.data)    
    food.food_group = food_group
    food.update_date = datetime.datetime.now()
    food.save()
    
    

    return redirect(url_for('food.food_information',
                            food_id=food.id))


@module.route('/delete_food/<food_id>', methods=['GET', 'POST'])
def delete_food(food_id):
    food = models.Food.objects.get(id=food_id)
    food.delete()

    return redirect(url_for('food.food_list'))


@module.route('/select_food/<food_id>', methods=['GET', 'POST'])
def select_food(food_id):
    form = SetTime()
    user = models.User.objects.get(id=current_user._get_current_object().id)
    food = models.Food.objects.get(id=food_id)
    if not form.validate_on_submit():
        return render_template('food/set_time.html',
                               form=form,
                               user=user)

    eating_log = models.EatingLog(user=user,
                                  food_name=food.name,
                                  calories=food.calories,
                                  category='Restaurant',
                                  food_infomation=food,
                                  created_date=datetime.datetime.now(),
                                  update_date=datetime.datetime.now())
    day = int(form.date_eat.data.strftime('%d'))
    month = int(form.date_eat.data.strftime('%m'))
    year = int(form.date_eat.data.strftime('%Y'))
    hour = int(form.time_eat.data.strftime('%H'))
    minute = int(form.time_eat.data.strftime('%M'))
    eating_log.datetime_eat = datetime.datetime(year, month, day, hour, minute)
    eating_log.save()
    
    if food.calories > 0:
        user.cal_limit -= food.calories
        user.cal_limit_per_week -= food.calories
        user.calories_in += food.calories
        if user.cal_limit < 0:
            user.cal_limit = 0
        if user.cal_limit_per_week < 0:
            user.cal_limit_per_week = 0

    if food.food_group.protein:
        user.food_group.protein = True
    if food.food_group.carbohydrate:
        user.food_group.carbohydrate = True
    if food.food_group.vitamin:
        user.food_group.vitamin = True
    if food.food_group.mineral:
        user.food_group.mineral = True
    if food.food_group.fat:
        user.food_group.fat = True

    user.save()

    return redirect(url_for('eating_log.eating_list'))


@module.route('/food_search_result', methods=["GET"])
@login_required
def search_result():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    search = request.args.get('search')
    food = models.Food.objects((Q(name__icontains=search) |
                                Q(description__icontains=search) |
                                Q(restaurant__icontains=search) |
                                Q(tags__icontains=search)))

    return render_template('/food/search_food.html',
                           user=user,
                           food=food)


@module.route('food_image/<food_id>/<file_name>', methods=["GET", "POST"])
def food_image(food_id, file_name):
    food = models.Food.objects.get(id=food_id)
    img = None
    if food.image.filename == file_name:
        img = food.image

    response = Response()
    if img:
        response = send_file(img, mimetype='image/jpg')
    else:
        response.status_code = 404

    return response