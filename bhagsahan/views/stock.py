from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask)
from .. import models
from bhagsahan.forms import StockForm, IncreaseStockForm
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Flask, redirect, url_for, session, request, jsonify
from flask_login import logout_user, current_user, login_required


import datetime


module = Blueprint('stock', __name__, url_prefix='/')



@module.route('/stock')
@login_required
def stock():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    stocks = models.Stock.objects()
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('stock/stock.html',
                           stocks=stocks,
                           user=user)


@module.route('/stock_info/<stock_id>', methods=['GET', 'POST'])
@login_required
def stock_info(stock_id):
    user = models.User.objects.get(id=current_user._get_current_object().id)
    stock = models.Stock.objects().get(id=stock_id) 
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('stock/stock_info.html',
                            stock=stock,
                            user=user)


@module.route('/add_stock', methods=['GET', 'POST'])
@login_required
def add_stock():
    form = StockForm()
    user = models.User.objects.get(id=current_user._get_current_object().id)
    if not user:
        return redirect(url_for('user.initial_user'))
    if not form.validate_on_submit():
        return render_template('stock/add_stock.html',
                               form=form,
                               user=user)
    
    new_srt = form.tags.data.strip()
    if new_srt.endswith(','):
        new_srt = new_srt[:-1]
    if new_srt.startswith(','):
        new_srt = new_srt[1:]
    ltags = [tag for tag in  new_srt.split(',') if tag != '']

    stock = models.Stock(name=form.name.data,
                         description=form.description.data,
                         tags=ltags,
                         volume=form.volume.data,
                         amount_type=form.amount_type.data,
                         stock_type=form.stock_type.data,
                         active=True,
                         created_date=datetime.datetime.now(),
                         update_date=datetime.datetime.now())

    nutritions = models.Nutrition.objects()
    for nutrition in nutritions:
        if nutrition.item.name == form.name.data:
            stock.nutrition = nutrition
            stock.save()
            return redirect(url_for('stock.stock_info',
                            stock_id=stock.id))

    item = models.Item(name=form.name.data,
                       description=form.description.data,
                       tags=ltags,
                       created_date=datetime.datetime.now(),
                       updated_date=datetime.datetime.now())
    item.save()
    
    allergen = models.Allergen()
    facts = models.NutritionFact()
    new_nutrition = models.Nutrition(item=item,
                                     allergen=allergen,
                                     facts=facts,
                                     created_date=datetime.datetime.now(),
                                     updated_date=datetime.datetime.now())
    new_nutrition.save()
    
    stock.nutrition = new_nutrition
    stock.save()

    return redirect(url_for('stock.stock_info',
                            stock_id=stock.id))


@module.route('/increase_stock/<stock_id>', methods=['GET', 'POST'])
@login_required
def increase_stock(stock_id):
    user = models.User.objects.get(id=current_user._get_current_object().id)
    stock = models.Stock.objects().get(id=stock_id) 
    form = IncreaseStockForm()

    if not user:
        return redirect(url_for('user.initial_user'))

    if not form.validate_on_submit():
        return render_template('stock/increase_stock.html',
                               form=form,
                               stock=stock,
                               user=user)
    
    stock.volume += form.volume.data
    stock.save()
    
    return redirect(url_for('stock.stock'))


@module.route('/edit_stock/<stock_id>', methods=['GET', 'POST'])
@login_required
def edit_stock(stock_id):
    form = StockForm()
    user = models.User.objects.get(id=current_user._get_current_object().id)
    stock = models.Stock.objects.get(id=stock_id)

    if not user:
        return redirect(url_for('user.initial_user'))

    if not form.validate_on_submit():
        form.name.data = stock.name
        form.description.data = stock.description
        form.tags.data = ','.join(stock.tags)
        form.volume.data = stock.volume
        form.amount_type.data = stock.amount_type
        form.stock_type.data = stock.stock_type
        return render_template('stock/edit_stock.html',
                               form=form,
                               stock=stock,
                               user=user)
    
    new_srt = form.tags.data.strip()
    if new_srt.endswith(','):
        new_srt = new_srt[:-1]
    if new_srt.startswith(','):
        new_srt = new_srt[1:]
    ltags = [tag for tag in  new_srt.split(',') if tag != '']

    print(form.name.data)
    stock.name = form.name.data
    stock.description = form.description.data
    stock.tags = ltags
    stock.volume = form.volume.data
    stock.amount_type = form.amount_type.data
    stock.stock_type = form.stock_type.data
    stock.update_date = datetime.datetime.now()
    stock.save()

    return redirect(url_for('stock.stock_info',
                            stock_id=stock.id))



@module.route('/delete_stock/<stock_id>', methods=['GET', 'POST'])
def delete_stock(stock_id):
    stock = models.Stock.objects.get(id=stock_id)
    stock.delete()
    return redirect(url_for('stock.stock'))


