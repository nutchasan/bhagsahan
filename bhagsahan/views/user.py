from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app,
                   Flask,
                   Response,
                   send_file)
from .. import models
from mongoengine.queryset.visitor import Q
from bhagsahan.forms import EditProfileForm, UpdateBMIForm
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Flask, redirect, url_for, session, request, jsonify
from flask_login import logout_user, current_user, login_required

from bokeh.io import show
from bokeh.palettes import Category20c
from bokeh.plotting import figure
from bokeh.transform import cumsum
from bokeh.embed import components
from bokeh.palettes import Spectral6
from bokeh.models import (ColumnDataSource,
                          FactorRange,
                          LabelSet,
                          Legend,
                          HoverTool,
                          LabelSet,
                          LegendItem,
                          ranges)

import datetime


module = Blueprint('user', __name__, url_prefix='/')


def make_pie_per_week():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    today = datetime.date.today()
    start_date = datetime.datetime.combine(today, datetime.time(0, 0))
    end_date = datetime.datetime.combine(today, datetime.time(23, 50))
    days = []
    bmi = []
    color_list = []

    for i in range(7):
        num = 0
        if i > 0:
            num += 1
        start_date = start_date - datetime.timedelta(days=num)
        end_date = end_date - datetime.timedelta(days=num)
        
        bmi_log = models.BMILog.objects(Q(user=user) &
                                        Q(created_date__lte=end_date) &
                                        Q(created_date__gte=start_date)).order_by('-id')
        if bmi_log:
            for item in bmi_log:
                bmi_value = (item.weight / ((item.height / 100) ** 2))
                bmi.append(round(bmi_value,2))
                days.append(item.created_date.strftime('%A'))
                break

    bmi = bmi[::-1]
    days = days[::-1]
    for i in bmi:
        if i >= 18.50 and i <= 22.90:
            color_list.append('#7cb342')
        elif i >= 23.00:
            color_list.append('#ef5350')
        else:
            color_list.append('#4db6ac')

    p = figure(x_range=days,
               y_range= ranges.Range1d(start=0,end=35),
               plot_height=300,
               title="BMI per week (7 days)",
               toolbar_location=None,
               tools="")

    p.vbar(x=days,
           top=bmi,
           width=0.9,
           color=color_list)
    
    
    source = ColumnDataSource(dict(x=days,y=bmi))
    labels = LabelSet(x='x', y='y', text='y', level='glyph',
             x_offset=-13.5, y_offset=0, source=source, render_mode='canvas')

    p.add_layout(labels)

    p.xgrid.grid_line_color = None
    p.y_range.start = 0

    return p


def make_pie_per_month():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    today = datetime.date.today()
    start_date = datetime.datetime.combine(today, datetime.time(0, 0))
    end_date = datetime.datetime.combine(today, datetime.time(23, 50))
    factors = []
    bmi_data = []
    bmi = []
    color_list = []

    for i in range(28):
        num = 0
        if i > 0:
            num += 1
        start_date = start_date - datetime.timedelta(days=num)
        end_date = end_date - datetime.timedelta(days=num)

        bmi_log = models.BMILog.objects(Q(user=user) &
                                        Q(created_date__lte=end_date) &
                                        Q(created_date__gte=start_date)).order_by('-id')

        if bmi_log:
            for item in bmi_log:
                data_tuple = ()
                if i <= 6: 
                    data_tuple = ('Week 4', item.created_date.strftime('%d-%m-%Y'))
                    factors.append(data_tuple)
                    bmi_value = (item.weight / ((item.height / 100) ** 2))
                    bmi_data.append(round(bmi_value,2))
                    break
                if i <= 13: 
                    data_tuple = ('Week 3', item.created_date.strftime('%d-%m-%Y'))
                    factors.append(data_tuple)
                    bmi_value = (item.weight / ((item.height / 100) ** 2))
                    bmi_data.append(round(bmi_value,2))
                    break
                if i <= 20: 
                    data_tuple = ('Week 2', item.created_date.strftime('%d-%m-%Y'))
                    factors.append(data_tuple)
                    bmi_value = (item.weight / ((item.height / 100) ** 2))
                    bmi_data.append(round(bmi_value,2))
                    break
                if i <= 27: 
                    data_tuple = ('Week 1', item.created_date.strftime('%d-%m-%Y'))
                    factors.append(data_tuple)
                    bmi_value = (item.weight / ((item.height / 100) ** 2))
                    bmi_data.append(round(bmi_value,2))
                    break
    
    bmi_data = bmi_data[::-1]
    factors = factors[::-1]
    for i in bmi_data:
        if i >= 18.50 and i <= 22.90:
            color_list.append('#7cb342')
        elif i >= 23.00:
            color_list.append('#ef5350')
        else:
            color_list.append('#4db6ac')

    p = figure(x_range=FactorRange(*factors),
               y_range= ranges.Range1d(start=0,end=35),
               plot_height=250,
               toolbar_location=None,
               tools="",
               title="BMI per month (28 days)",)

    p.vbar(x=factors,
           top=bmi_data,
           width=0.9,
           color=color_list)

    p.y_range.start = 0
    p.x_range.range_padding = 0.1
    p.xaxis.major_label_orientation = 1
    p.xgrid.grid_line_color = None

    return p



@module.route('/initial_user', methods=['GET', 'POST'])
@login_required
def initial_user():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    form = EditProfileForm(obj=user)
    if user.is_init_profile:
        return redirect(url_for('main.index'))

    if not form.validate_on_submit():
        form.firstname.data = user.firstname
        form.lastname.data = user.lastname
        return render_template('user/initial_user.html',
                            form=form)

    user.firstname=form.firstname.data
    user.lastname=form.lastname.data
    user.age=form.age.data
    user.sex=form.sex.data
    user.weight=form.weight.data
    user.height=form.height.data
    user.cal_limit=2000
    user.cal_limit_per_week=14000
    user.calories_out=0
    user.bmi=(form.weight.data / ((form.height.data / 100) ** 2))
    user.created_date=datetime.datetime.now()
    user.update_date=datetime.datetime.now()
    user.today_date=datetime.datetime.now()
    user.is_init_profile = True
    allergen = models.UserAllergen(eggs=form.eggs.data,
                                  fish=form.fish.data,
                                  gluten=form.gluten.data,
                                  milk=form.milk.data,
                                  peanuts=form.peanuts.data,
                                  shellfish=form.shellfish.data,
                                  soybeans=form.soybeans.data,
                                  tree_nuts=form.tree_nuts.data,
                                  wheat=form.wheat.data)

    user.allergen = allergen

    if form.profile_image.data:
        user.profile_image.put(form.profile_image.data,
                                filename=form.profile_image.data.filename)
    user.save()
    # return redirect(url_for('oauth.oauth'))
    return redirect(url_for('main.index'))


@module.route('/profile')
@login_required
def profile():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    if not user:
        return redirect(url_for('user.initial_user'))
    return render_template('user/profile.html',
                           user=user)
    

@module.route('/bmi')
@login_required
def bmi():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    if not user:
        return redirect(url_for('user.initial_user'))
    
    week_pie = make_pie_per_week()
    month_pie = make_pie_per_month()
    week_script, week_div = components(week_pie)
    month_script, month_div = components(month_pie)    
    return render_template('user/bmi.html',
                           user=user,
                           week_div=week_div,
                           week_script=week_script,
                           month_script=month_script,
                           month_div=month_div)


@module.route('/update_bmi', methods=['GET', 'POST'])
@login_required
def update_bmi():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    form = UpdateBMIForm()
    if not user:
        return redirect(url_for('user.initial_user'))
    if not form.validate_on_submit():
        form.weight.data = user.weight
        form.height.data = user.height
        return render_template('user/update_bmi.html',
                               form=form,
                               user=user)

    user.weight = form.weight.data
    user.height = form.height.data 
    user.bmi = (form.weight.data / ((form.height.data / 100) ** 2))
    user.save()

    new_bmi = models.BMILog(user=user,
                            weight=form.weight.data,
                            height=form.height.data,
                            created_date=datetime.datetime.now())
    new_bmi.save()


    return redirect(url_for('user.bmi'))


@module.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    form = EditProfileForm()
    if not user:
        return redirect(url_for('user.initial_user'))
    if not form.validate_on_submit():
        form.firstname.data = user.firstname
        form.lastname.data = user.lastname
        form.age.data = user.age
        form.sex.data = user.sex
        form.weight.data = user.weight
        form.height.data  = user.height
        if user.allergen:
            form.eggs.data = user.allergen.eggs
            form.fish.data = user.allergen.fish
            form.gluten.data = user.allergen.gluten
            form.milk.data = user.allergen.milk
            form.peanuts.data = user.allergen.peanuts
            form.shellfish.data = user.allergen.shellfish
            form.soybeans.data = user.allergen.soybeans
            form.tree_nuts.data = user.allergen.tree_nuts
            form.wheat.data = user.allergen.wheat
        return render_template('user/edit.html',
                               form=form,
                               user=user)


    user.firstname = form.firstname.data
    user.lastname = form.lastname.data
    user.age = form.age.data
    user.sex = form.sex.data
    user.weight = form.weight.data
    user.height = form.height.data 
    user.bmi = (form.weight.data / ((form.height.data / 100) ** 2))

    allergen = models.UserAllergen(eggs=form.eggs.data,
                                  fish=form.fish.data,
                                  gluten=form.gluten.data,
                                  milk=form.milk.data,
                                  peanuts=form.peanuts.data,
                                  shellfish=form.shellfish.data,
                                  soybeans=form.soybeans.data,
                                  tree_nuts=form.tree_nuts.data,
                                  wheat=form.wheat.data)
    user.allergen = allergen
    user.update_date=datetime.datetime.now()
    if form.profile_image.data:
        user.profile_image.replace(form.profile_image.data,
                                   filename=form.profile_image.data.filename)
    user.save()

    new_bmi = models.BMILog(user=user,
                            weight=form.weight.data,
                            height=form.height.data,
                            created_date=datetime.datetime.now())
    new_bmi.save()

    return redirect(url_for('main.index'))


@module.route('/user_image/<file_name>', methods=["GET", "POST"])
@login_required
def user_image(file_name):
    user = models.User.objects.get(id=current_user._get_current_object().id)
    img = None
    if user.profile_image.filename == file_name:
        img = user.profile_image
    response = Response()
    if img:
        response = send_file(img, mimetype='image/jpg')
    else:
        response.status_code = 404

    return response


@module.route('/edit_activity_tracker', methods=['GET', 'POST'])
@login_required
def edit_activity_tracker():
    user = models.User.objects.get(id=current_user._get_current_object().id)
    if not user:
        return redirect(url_for('user.initial_user'))
    # if not form.validate_on_submit():
    #     return render_template('user/activity_tracker.html',
    #                             user=user)
    return render_template('user/activity_tracker.html',
                            user=user)
