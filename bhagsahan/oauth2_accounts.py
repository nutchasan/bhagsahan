from flask import g, config, session, redirect, url_for
from flask_login import current_user, login_user
# from flask_user import current_user, login_user
from authlib.flask.client import OAuth
import loginpass

from . import models
import mongoengine as me

import datetime

def fetch_token(name):
    token = models.OAuthTokenAccount.objects(name=name,
            user=current_user._get_current_object()).first()
    return token.to_dict()

def update_token(name, token):
    item = models.OAuth2Token(name=name, user=current_user._get_current_object()).first()
    item.token_type = token.get('token_type', 'Bearer')
    item.access_token = token.get('access_token')
    item.refresh_token = token.get('refresh_token')
    item.expires = datetime.datetime.utcfromtimestamp(token.get('expires_at'))

    item.save()
    return item


oauth2_client = OAuth()


def handle_authorize_google(remote, token, user_info):

    if not user_info:
        return redirect(url_for('accounts.login'))
    print('google login success', user_info)
    user = models.User.objects(me.Q(email=user_info.get('email'))).first()
    if not user:
        user = models.User(
            email=user_info.get('email'),
            firstname=user_info.get('given_name'),
            lastname=user_info.get('family_name'))
        user.save()

    login_user(user, remember=True)

    if token:
        oauth2token = models.OAuthTokenAccount(
                name=remote.name,
                user=user,
                access_token=token.get('access_token'),
                token_type=token.get('token_type'),
                refresh_token=token.get('refresh_token', None),
                expires=datetime.datetime.utcfromtimestamp(
                                                           token.get('expires_in'))
                )
        oauth2token.save()

    return redirect(url_for('main.index'))


def init_oauth(app):
    oauth2_client.init_app(app,
                           fetch_token=fetch_token,
                           update_token=update_token)
    google_bp = loginpass.create_flask_blueprint(
            loginpass.Google,
            oauth2_client,
            handle_authorize_google)

    app.register_blueprint(google_bp, url_prefix='/google')
