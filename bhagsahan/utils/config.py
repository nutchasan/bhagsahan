import os
import flask

settings = None

def get_settings():
    global settings
    if not settings:
        filename = os.environ.get('BHAGSAHAN_SETTINGS', None)
        if filename is None:
            print('This program require BHAGSAHAN_SETTINGS environment')
            return
        print(filename)
        file_path = os.path.join(
                os.path.dirname(os.path.abspath(__file__)), '../../')
        settings = flask.config.Config(file_path)
        settings.from_object('bhagsahan.default_settings')
        settings.from_envvar('BHAGSAHAN_SETTINGS', silent=True)
    return settings
