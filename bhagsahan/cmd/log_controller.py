from bhagsahan import log_controller

def main():
    server = log_controller.create_server()
    server.run()
