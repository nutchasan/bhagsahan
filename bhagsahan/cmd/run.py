import bhagsahan
def main():
    # options = program_options.get_program_options(default_port=8080)

    app = bhagsahan.create_app()

    # program_options.initial_profile(app, options)

    app.run(
        debug=True,
        host='0.0.0.0',
        port=8080
    )
