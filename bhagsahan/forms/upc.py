from flask_wtf import FlaskForm
from wtforms import fields
# from wtforms.fields import html5
from wtforms import validators


class UPCForm(FlaskForm):
    upc = fields.TextField('UPC Code',
                                 validators=[validators.InputRequired()])
    
