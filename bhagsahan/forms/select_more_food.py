from flask_wtf import FlaskForm
from wtforms import fields
from wtforms import validators
import datetime


class SelectMoreFoodForm(FlaskForm):
    food_name = fields.TextField('Food Name',
                                 validators=[validators.InputRequired()])
    cal_in = fields.IntegerField('Calories',
                                 validators=[validators.InputRequired()])
    date_eat = fields.DateField('Date to eat',
                                    format='%d/%m/%Y',
                                    default=datetime.date.today(),
                                    validators=[validators.Optional()])
    time_eat = fields.TimeField('Time to eat',
                                    format='%H:%M',
                                    default=datetime.datetime.now(),
                                    validators=[validators.Optional()])

class SetTime(FlaskForm):
    date_eat = fields.DateField('Date to eat',
                                    format='%d/%m/%Y',
                                    default=datetime.date.today(),
                                    validators=[validators.Optional()])
    time_eat = fields.TimeField('Time to eat',
                                    format='%H:%M',
                                    default=datetime.datetime.now(),
                                    validators=[validators.Optional()])
