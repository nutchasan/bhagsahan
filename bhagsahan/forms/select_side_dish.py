from flask_wtf import FlaskForm
from wtforms import fields
from wtforms import validators
import datetime

class NumberOfPelpleForm(FlaskForm):
    num_people = fields.SelectField('Number of pelple',
                                            choices=[(1, '1'),
                                                        (2, '2'),
                                                        (3, '3'),
                                                        (4, '4'),
                                                        (5, '5'),
                                                        (6, '6'),
                                                        (7, '7'),
                                                        (8, '8'),
                                                        (9, '9'),
                                                        (10, '10')],
                                            coerce=int)