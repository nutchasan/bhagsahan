from flask_wtf import FlaskForm
from wtforms import fields, Form
from wtforms import validators


class IngredientForm(Form):
    in_name = fields.TextField('Name',
                            validators=[validators.InputRequired()])
    volume = fields.IntegerField('Volume')
    amount_type = fields.TextField('Amount Type')


class MethodForm(Form):
    info = fields.TextField('Method',
                            validators=[validators.InputRequired()])


class RecipeForm(FlaskForm):
    name = fields.TextField('Name',
                             validators=[validators.InputRequired()])
    description = fields.TextField('Description')
    tags = fields.TextField('Tags')
    unit = fields.TextField('Unit')
    calories = fields.IntegerField('Calories',
                               validators=[validators.InputRequired()])

    ingredient_num = fields.SelectField('Number of ingredient',
                                            choices=[(1, '1'),
                                                        (2, '2'),
                                                        (3, '3'),
                                                        (4, '4'),
                                                        (5, '5'),
                                                        (6, '6'),
                                                        (7, '7'),
                                                        (8, '8'),
                                                        (9, '9'),
                                                        (10, '10')],
                                            coerce=int)
    methods_num = fields.SelectField('Number of method',
                                            choices=[(1, '1'),
                                                        (2, '2'),
                                                        (3, '3'),
                                                        (4, '4'),
                                                        (5, '5'),
                                                        (6, '6'),
                                                        (7, '7'),
                                                        (8, '8'),
                                                        (9, '9'),
                                                        (10, '10')],
                                            coerce=int)
    
    food_type = fields.SelectField('Number of method',
                                            choices=[('A la carte', 'A la carte'),
                                                     ('Side dish', 'Side dish'),
                                                    ])


class ListForm(FlaskForm):
    ingredients = fields.FieldList(fields.FormField(IngredientForm), min_entries=1)
    methods =  fields.FieldList(fields.FormField(MethodForm), min_entries=1)


class ImageRecipeForm(FlaskForm):
    image = fields.FileField('Image')
