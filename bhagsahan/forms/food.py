from flask_wtf import FlaskForm
from wtforms import fields
from wtforms import validators
import datetime


class FoodForm(FlaskForm):
    name = fields.TextField('Food Name',
                             validators=[validators.InputRequired()])
    description = fields.TextField('Description')
    restaurant = fields.TextField('Resturant',
                                  validators=[validators.InputRequired()])
    calories = fields.IntegerField('Calories',
                                            validators=[validators.InputRequired()])
    tags = fields.TextField('Tags')
    image = fields.FileField('Image')
    
    protein = fields.BooleanField('Protein')
    carbohydrate = fields.BooleanField('Carbohydrate')
    vitamin = fields.BooleanField('Vitamin')
    mineral = fields.BooleanField('Mineral')
    fat = fields.BooleanField('Fat')
