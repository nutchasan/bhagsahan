from flask_wtf import FlaskForm
from wtforms import fields
from wtforms.fields import html5
from wtforms import validators

class LoginForm(FlaskForm):
    email = html5.EmailField('Email', validators=[validators.InputRequired(),
                             validators.Email()])
    password = fields.PasswordField('Password',
                                    validators=[validators.InputRequired()])

class EditProfileForm(FlaskForm):
    firstname = fields.TextField('Firstname',
                                 validators=[validators.InputRequired(),
                                             validators.Length(min=3)])
    lastname = fields.TextField('Lastname',
                                validators=[validators.InputRequired(),
                                            validators.Length(min=3)])
    age = fields.IntegerField('Age',
                               validators=[validators.InputRequired()])
    sex = fields.SelectField('Sex',
                             choices=[('male', 'Male'),
                                     ('female', 'Female')])
    weight = fields.FloatField('Weight (kg.)',
                                  validators=[validators.InputRequired()])
    height = fields.FloatField('Height (cm.)',
                                  validators=[validators.InputRequired()])
    profile_image = fields.FileField('Profile Image')
    
    eggs = fields.BooleanField('Eggs') 
    fish = fields.BooleanField('Fish')
    gluten = fields.BooleanField('Gluten')
    milk = fields.BooleanField('Milk')
    peanuts = fields.BooleanField('Peanuts')
    shellfish = fields.BooleanField('Shellfish')
    soybeans = fields.BooleanField('Soybeans')
    tree_nuts = fields.BooleanField('Tree nuts')
    wheat = fields.BooleanField('Wheat')


class UpdateBMIForm(FlaskForm):
    weight = fields.FloatField('Weight (kg.)',
                                  validators=[validators.InputRequired()])
    height = fields.FloatField('Height (cm.)',
                                  validators=[validators.InputRequired()])
