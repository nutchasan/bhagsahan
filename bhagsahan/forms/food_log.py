from flask_wtf import FlaskForm
from wtforms import fields
from wtforms import validators


class FoodLogForm(FlaskForm):
    #Item
    name = fields.TextField('Name',
                             validators=[validators.InputRequired()])
    description = fields.TextField('Description')
    tags = fields.TextField('Tags')
    upc = fields.TextField('UPC')
    color = fields.TextField('Color')
    size = fields.TextField('Size')
    weight = fields.TextField('Weight')
    dimension = fields.TextField('Dimension')
    image = fields.FileField('Image')
    nutrition_image = fields.FileField('Nutrition Image')
    ingredient_image = fields.FileField('Ingredient Image')
    upc_image = fields.FileField('UPC Image')
    status = fields.SelectField('Status',
                                choices=[('active', 'Active'),
                                         ('deactive', 'Deactive')])

    #Allergen
    eggs = fields.BooleanField('Eggs') 
    fish = fields.BooleanField('Fish')
    gluten = fields.BooleanField('Gluten')
    milk = fields.BooleanField('Milk')
    peanuts = fields.BooleanField('Peanuts')
    shellfish = fields.BooleanField('Shellfish')
    soybeans = fields.BooleanField('Soybeans')
    tree_nuts = fields.BooleanField('Tree nuts')
    wheat = fields.BooleanField('Wheat')
    
    #FoodGroup
    protein = fields.BooleanField('Protein')
    carbohydrate = fields.BooleanField('Carbohydrate')
    vitamin = fields.BooleanField('Vitamin')
    mineral = fields.BooleanField('Mineral')
    fat = fields.BooleanField('Fat')

    #NutritionFact
    calcium_dv = fields.IntegerField('Calcium DV', default=0)
    calories = fields.IntegerField('Calories', default=0)
    calories_from_fat = fields.IntegerField('Fat Calories', default=0)
    cholesterol = fields.IntegerField('Cholesterol', default=0)
    dietary_fiber = fields.IntegerField('Dietary Fiber', default=0)
    ingredient_statement = fields.StringField('Ingredient Statement')
    iron_dv = fields.IntegerField('Iron DV', default=0)
    monounsaturated_fat = fields.IntegerField('Monounsaturated Fat', default=0)
    polyunsaturated_fat = fields.IntegerField('Polyunsaturated Fat', default=0)
    proteins = fields.IntegerField('Protein', default=0)
    refuse_pct = fields.IntegerField('Refuse PCT', default=0)
    saturated_fat = fields.IntegerField('Saturated Fat', default=0)
    serving_size_quantity = fields.IntegerField('Serving Size Quantity', default=0)
    serving_size_unit = fields.StringField('Serving Size Unit')
    serving_weight_grams = fields.IntegerField('Serving Weight Grams', default=0)
    servings_per_container = fields.IntegerField('Servings Per Container', default=0)
    sodium = fields.IntegerField('Sodium', default=0)
    sugars = fields.IntegerField('Sugars', default=0)
    total_carbohydrate = fields.IntegerField('Total Carbohydrate', default=0)
    total_fat = fields.IntegerField('Total Fat', default=0)
    trans_fatty_acid = fields.IntegerField('Trans Fatty Acid', default=0)
    vitamin_a_dv = fields.IntegerField('Vitamin A DV', default=0)
    vitamin_c_dv = fields.IntegerField('Vitamin C DV', default=0)
    water_grams = fields.IntegerField('Water Grams', default=0)
