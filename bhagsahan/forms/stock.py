from flask_wtf import FlaskForm
from wtforms import fields
from wtforms import validators


class StockForm(FlaskForm):
    name = fields.TextField('Name',
                             validators=[validators.InputRequired()])
    description = fields.TextField('Description')
    tags = fields.TextField('Tags')
    
    volume = fields.IntegerField('Volume')
    amount_type = fields.TextField('Amount Type')
    
    stock_type = fields.SelectField('Stock type',
                                choices=[('instant food', 'Instant food'),
                                         ('ingredient', 'Ingredient')])

# class AddItemForm(FlaskForm):
#     volume = fields.IntegerField('Volume')

class IncreaseStockForm(FlaskForm):
    volume = fields.IntegerField('Volume')

    