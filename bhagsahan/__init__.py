__version__ = '0.1'

from flask import Flask
import bhagsahan
from . import views
from . import models
from . import oauth2_accounts
from . import acl


def create_app():
    app = Flask(__name__)
    app.config.from_object('bhagsahan.default_settings')
    app.config.from_envvar('BHAGSAHAN_SETTINGS', silent=True)

    models.init_db(app)
    acl.init_acl(app)
    oauth2_accounts.init_oauth(app)
    
    views.register_blueprint(app)

    return app

