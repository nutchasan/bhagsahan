import datetime
import mongoengine as me
from . import users


class OAuthToken(me.Document):
    user = me.ReferenceField(users.User, dbref=True)
    access_token = me.StringField(required=True)
    expires_in = me.IntField(required=True, default=0)
    refresh_token = me.StringField()
    scope = me.StringField()
    token_type = me.StringField()
    user_id = me.StringField()
    expires_at = me.IntField(required=True, default=0)
    meta = {'collection': 'oauth_tokens'}

    def to_dict(self):
        return dict(
            access_token=self.access_token,
            expires_in=self.expires_in,
            refresh_token=self.refresh_token,
            scope = self.scope,
            token_type=self.token_type,
            user_id = self.user_id,
            expires_at=self.expires_at,
        )


class OAuthTokenAccount(me.Document):
    user = me.ReferenceField(users.User, dbref=True)
    name = me.StringField(required=True)

    token_type = me.StringField()
    access_token = me.StringField(required=True)
    # refresh_token or access_token_secret
    refresh_token = me.StringField()
    expires = me.DateTimeField(required=True,
                               default=datetime.datetime.now)

    meta = {'collection': 'oauth_tokens_accounts'}

    @property
    def expires_at(self):
        return self.expires.timestamp()

    def to_dict(self):
        return dict(
            access_token=self.access_token,
            token_type=self.token_type,
            refresh_token=self.refresh_token,
            expires_at=self.expires_at,
        )
