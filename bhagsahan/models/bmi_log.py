import mongoengine as me
import datetime
from . import users


class BMILog(me.Document):
    meta = {'collection': 'bmi_logs'}

    user = me.ReferenceField(users.User, dbref=True)
    weight = me.FloatField()
    height = me.FloatField()
    bmi = me.FloatField()

    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
