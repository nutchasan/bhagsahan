import mongoengine as me
import datetime


class Ingredient(me.EmbeddedDocument):
    name = me.StringField()
    volume = me.IntField()
    amount_type = me.StringField()


class Recipe(me.Document):
    meta = {'collection': 'recipes'}

    name = me.StringField(required=True)
    description = me.StringField()
    tags = me.ListField(me.StringField())

    ingredients = me.ListField(me.EmbeddedDocumentField(Ingredient))
    methods = me.ListField(me.StringField())
    unit = me.StringField()
    calories = me.IntField(required=True, default=0)

    image = me.FileField()
    
    status = me.BooleanField(required=False, default=False)
    ingredient_status = me.BooleanField(required=False, default=False)
    allergen_status = me.BooleanField(required=False, default=False)
    side_dish = me.BooleanField(required=False, default=False)

    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
    update_date = me.DateTimeField(required=True,
                                   default=datetime.datetime.utcnow)
