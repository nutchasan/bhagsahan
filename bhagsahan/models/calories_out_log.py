import mongoengine as me
import datetime
from . import users


class CaloriesOutLog(me.Document):
    meta = {'collection': 'calories_out_logs'}

    user = me.ReferenceField(users.User, dbref=True)
    calories_out = me.IntField()

    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
