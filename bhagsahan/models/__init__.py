from flask_mongoengine import MongoEngine
from .users import User, UserAllergen, UserFoodGroup
from .nutrition import Allergen, NutritionFact, Item, Nutrition, FoodGroup
from .stock import Stock
from .recipes import Ingredient, Recipe
from .oauth import OAuthToken, OAuthTokenAccount
from .eating_log import EatingLog
from .food import Food, RestaurantFoodGroup
from .bmi_log import BMILog
from .calories_out_log import CaloriesOutLog
from .calories_in_log import CaloriesInLog


db = MongoEngine()

__all__ = [User, Allergen, Nutrition, Item,
           NutritionFact, Stock, Ingredient,
           Recipe, OAuthToken, UserAllergen,
           OAuthTokenAccount, EatingLog, Food,
           BMILog, CaloriesOutLog, CaloriesOutLog,
           UserFoodGroup, FoodGroup, RestaurantFoodGroup]


def init_db(app):
    db.init_app(app)

def init_mongoengine(settings):
    import mongoengine as me
    dbname = settings.get('MONGODB_DB')
    host = settings.get('MONGODB_HOST', 'localhost')
    port = int(settings.get('MONGODB_PORT', '27017'))
    username = settings.get('MONGODB_USERNAME', '')
    password = settings.get('MONGODB_PASSWORD', '')
    me.connect(db=dbname,
               host=host,
               port=port,
               username=username,
               password=password)
