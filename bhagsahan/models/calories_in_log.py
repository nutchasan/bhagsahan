import mongoengine as me
import datetime
from . import users


class CaloriesInLog(me.Document):
    meta = {'collection': 'calories_in_logs'}

    user = me.ReferenceField(users.User, dbref=True)
    calories_in = me.IntField()
    
    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
    update_date = me.DateTimeField(required=True,
                                   default=datetime.datetime.utcnow)