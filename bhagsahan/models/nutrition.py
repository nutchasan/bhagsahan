import mongoengine as me
import datetime

class Allergen(me.EmbeddedDocument):
    eggs = me.BooleanField(required=True, default=False)
    fish = me.BooleanField(required=True, default=False)
    gluten = me.BooleanField(required=True, default=False)
    milk = me.BooleanField(required=True, default=False)
    peanuts = me.BooleanField(required=True, default=False)
    shellfish = me.BooleanField(required=True, default=False)
    soybeans = me.BooleanField(required=True, default=False)
    tree_nuts = me.BooleanField(required=True, default=False)
    wheat = me.BooleanField(required=True, default=False)


class NutritionFact(me.EmbeddedDocument):
    calcium_dv = me.IntField(required=True, default=0)
    calories = me.IntField(required=True, default=0)
    calories_from_fat = me.IntField(required=True, default=0)
    cholesterol = me.IntField(required=True, default=0)
    dietary_fiber = me.IntField(required=True, default=0)
    ingredient_statement = me.StringField(required=True, default='')
    iron_dv = me.IntField(required=True, default=0)
    monounsaturated_fat = me.IntField(required=True, default=0)
    polyunsaturated_fat = me.IntField(required=True, default=0)
    protein = me.IntField(required=True, default=0)
    refuse_pct = me.IntField(required=True, default=0)
    saturated_fat = me.IntField(required=True, default=0)
    serving_size_quantity = me.IntField(required=True, default=0)
    serving_size_unit = me.StringField(required=True, default='')
    serving_weight_grams = me.IntField(required=True, default=0)
    servings_per_container = me.IntField(required=True, default=0)
    sodium = me.IntField(required=True, default=0)
    sugars = me.IntField(required=True, default=0)
    total_carbohydrate = me.IntField(required=True, default=0)
    total_fat = me.IntField(required=True, default=0)
    trans_fatty_acid = me.IntField(required=True, default=0)
    vitamin_a_dv = me.IntField(required=True, default=0)
    vitamin_c_dv = me.IntField(required=True, default=0)
    water_grams = me.IntField(required=True, default=0)
    

class FoodGroup(me.EmbeddedDocument):
    protein = me.BooleanField(required=True, default=False)
    carbohydrate = me.BooleanField(required=True, default=False)
    vitamin = me.BooleanField(required=True, default=False)
    mineral = me.BooleanField(required=True, default=False)
    fat = me.BooleanField(required=True, default=False)


class Item(me.Document):
    name = me.StringField(required=True)
    description = me.StringField()
    tags = me.ListField(me.StringField())
    upc = me.StringField(uniqued=True)
    color = me.StringField()
    size = me.StringField()
    weight = me.StringField()
    dimension = me.StringField()

    image = me.FileField()
    nutrition_image = me.FileField()
    ingredient_image = me.FileField()
    upc_image = me.FileField()

    status = me.StringField(required=True, default='deactivate')

    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
    updated_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow,
                                    auto_now=True)

    meta = {'collection': 'items'}


class Nutrition(me.Document):
    item = me.ReferenceField(Item, dbref=True)
    allergen = me.EmbeddedDocumentField(Allergen)
    facts = me.EmbeddedDocumentField(NutritionFact)
    food_group = me.EmbeddedDocumentField(FoodGroup)
    

    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
    updated_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow,
                                    auto_now=True)

    meta = {'collection': 'nutritions'}