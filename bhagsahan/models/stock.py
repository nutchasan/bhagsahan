import mongoengine as me
import datetime
from .nutrition import Nutrition


class Stock(me.Document):
    meta = {'collection': 'stock'}

    name = me.StringField(required=True)
    description = me.StringField()

    tags = me.ListField(me.StringField())
    volume = me.IntField()
    amount_type = me.StringField()
    stock_type = me.StringField()
    
    nutrition = me.ReferenceField(Nutrition, dbref=True)

    active = me.BooleanField(default=False)
    
    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
    update_date = me.DateTimeField(required=True,
                                   default=datetime.datetime.utcnow)
