import mongoengine as me
import datetime
from . import users
from . import recipes
from . import food


class EatingLog(me.Document):
    meta = {'collection': 'eating_logs'}

    user = me.ReferenceField(users.User, dbref=True)
    food_name = me.StringField(required=True)
    calories = me.IntField(required=True)
    category = me.StringField(required=True)
    food_infomation = me.ReferenceField(food.Food, dbref=True)
    recipe_infomation = me.ReferenceField(recipes.Recipe, dbref=True)
    datetime_eat = me.DateTimeField(required=True)

    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
    update_date = me.DateTimeField(required=True,
                                   default=datetime.datetime.utcnow)

