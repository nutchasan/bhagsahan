import mongoengine as me
import datetime
from . import users

class RestaurantFoodGroup(me.EmbeddedDocument):
    protein = me.BooleanField(required=True, default=False)
    carbohydrate = me.BooleanField(required=True, default=False)
    vitamin = me.BooleanField(required=True, default=False)
    mineral = me.BooleanField(required=True, default=False)
    fat = me.BooleanField(required=True, default=False)

class Food(me.Document):
    meta = {'collection': 'food'}
    
    name = me.StringField(required=True)
    description = me.StringField()
    restaurant = me.StringField()
    calories = me.IntField(required=True, default=0)
    tags = me.ListField(me.StringField())
    image = me.FileField()
    food_group = me.EmbeddedDocumentField(RestaurantFoodGroup)
    user_created = me.ReferenceField(users.User, dbref=True)
    
    
    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
    update_date = me.DateTimeField(required=True,
                                   default=datetime.datetime.utcnow)