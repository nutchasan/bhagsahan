import mongoengine as me
import datetime
from flask_login import UserMixin

end_week_date = datetime.datetime.now() + datetime.timedelta(days=7)


class UserAllergen(me.EmbeddedDocument):
    eggs = me.BooleanField(required=True, default=False)
    fish = me.BooleanField(required=True, default=False)
    gluten = me.BooleanField(required=True, default=False)
    milk = me.BooleanField(required=True, default=False)
    peanuts = me.BooleanField(required=True, default=False)
    shellfish = me.BooleanField(required=True, default=False)
    soybeans = me.BooleanField(required=True, default=False)
    tree_nuts = me.BooleanField(required=True, default=False)
    wheat = me.BooleanField(required=True, default=False)
    
class UserFoodGroup(me.EmbeddedDocument):
    protein = me.BooleanField(required=True, default=False)
    carbohydrate = me.BooleanField(required=True, default=False)
    vitamin = me.BooleanField(required=True, default=False)
    mineral = me.BooleanField(required=True, default=False)
    fat = me.BooleanField(required=True, default=False)


class User(me.Document, UserMixin):
    meta = {'collection': 'users'}
    encoded_id = me.StringField()
    firstname = me.StringField()
    lastname = me.StringField()
    email = me.StringField()
    age = me.IntField()
    sex = me.StringField()
    weight = me.IntField()
    height = me.IntField()
    cal_limit = me.IntField(required=True, default=2000)
    cal_limit_per_week = me.IntField(required=True, default=14000)
    calories_out = me.IntField()
    calories_in = me.IntField(required=True, default=0)
    activity_calories = me.IntField()
    bmi = me.FloatField()
    calories_week_summation = me.IntField()
    profile_image = me.FileField()

    is_init_profile = me.BooleanField(required=True,
                                      default=False)

    cal_limit_over = me.BooleanField(required=True,
                                     default=False)

    allergen = me.EmbeddedDocumentField(UserAllergen)
    food_group = me.EmbeddedDocumentField(UserFoodGroup)

    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
    update_date = me.DateTimeField(required=True,
                                   default=datetime.datetime.utcnow)
    today_date = me.DateTimeField(required=True,
                                   default=datetime.datetime.utcnow)
    end_week_date = me.DateTimeField(required=True,
                                     default=end_week_date)
